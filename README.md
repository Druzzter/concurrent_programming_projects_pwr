Concurrent Programming project:

The way of an idea or task:
1. BOSS sending tasks to the LIST GUARD in company.
2. LIST GUARD adds tasks to the list.
3. WORKER asks LIST GUARD if there's some tasks to do.
4. WORKER receives task and solves it.
5. WORKER sending solved task to the WAREHOUSE GUARD.
6. WAREHOUSE GUARD gets product and adds it to the warehouse.
7. CLIENT asks WAREHOUSE GUARD if there is any product.
8. CLIENT receives product from WAREHOUSE GUARD. 

Realtime modulation of the company:
['-' / '+']  - to speed up or slow down the time of work.
['y']        - to spy statistics of list, warehouse, speed, etc.
['1' / '0'] - to turn on verbose/silent mode.
['n']        - to turn off task OBSERVER.
[ctrl+c]   - END PROGRAM.
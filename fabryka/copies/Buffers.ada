
package body Buffers is

	 protected body NewBuffer is
			entry Add(I:in Integer) when Count<Index'Last is
			begin 
				B(In_P):=I;
				Count:=Count+1;
				In_P:=In_P+1;
			end Add;
			
			entry Del(I: out Integer) when Count>0 is
			begin 
				I:=B(Out_P);
				Count:=Count-1;
				Out_P:=Out_P+1;
			end Del;
			
		end NewBuffer;
   --Obiekt chroniony
   -- Piszac protected type mozna zdefiniowac typ chroniony

   protected body Buffer is
      entry Append (I: in Integer; J :in Integer; Y: in String) when Count<Index'Last is
      begin
         B(In_P).x:=I;
				 B(In_P).y:=J;
				 B(In_P).z:=Y;
         Count:=Count+1;
         In_P:=In_P+1;
      end Append;

      entry Take (I: out Integer) when Count>0 is
      begin
         I:=B(Out_P).x;
         Count:=Count-1;
         Out_P:=Out_P+1;
      end Take;

   end Buffer;

end Buffers;
with Ada.Text_IO, declarations, buffers; 
use Ada.Text_IO, declarations, buffers;

procedure Boss_Worker_Client is
	 
	 task Boss is
		entry Start(F:in Integer);
		entry Finish(G: out Integer);
	 end Boss;
	 task body Boss is
				 X:Integer:=2;
				 OP:String(1..2):="+";
	 begin
			for N in 1..NoOfItem loop
				-- accept Start(F :in Integer) do
				 
				 Put_Line("BOSS: Product concept added" & Integer'Image(N)&"as "&Integer'Image(N)&" "&OP&" "&Integer'Image(X));
				 Buffer.Append(N,X,OP);
				 -- end Start;
				 delay DelayTimer*SPEED;
			end loop;
	 end Boss;
	 
   task Worker is
		entry Start(F:in Integer);
		entry Finish(G: out Integer);
	 end Worker;
   task body Worker is
      F:Integer;
      I:Natural:=0;
   begin
      loop
				 I:=I+1;
				 F:=I;
				-- accept Start(F:in Integer) do
				 Put_Line("WORKER: Making product" & Integer'Image(I));
				 Buffer.Take(I);
         Put_Line("WORKER Selling product" & Integer'Image(I));
				 NewBuffer.Add(I);
        delay DelayTimer+MultiAdder*SPEED;
				 
				-- end Start;
				 --G:=I;
					-- accept Finish(G:out Integer) do
						null;
					-- end Finish;
					exit when I=NoOfItem;
      end loop;
   end Worker;
	 
	 task Client is
		entry Start(L:in Integer);
		entry Finish(G:out Integer);
	 end Client;
	 task body Client is
			L: Integer;
			I: Natural:=0;
		begin 
			loop
				I:=I+1;
				L:=I;
				-- accept Start(L:in Integer) do
				Put_Line("CLIENT: Buying" & Integer'Image(I));
				NewBuffer.Del(I);
				-- end Start;
				delay DelayTimer*MultiAdder+MultiAdder*SPEED;
				 
				
				-- accept Finish(G: out Integer) do
					null;
				-- end Finish;
				
				exit when I=NoOfItem;
			end loop;
	 end Client;

begin
   null; --Uruchomione sa dwa zadania, ktore konkuruja o zasob
end Boss_Worker_Client;


--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
package Client is
	 task type Client is
		entry Start(L:in Natural);
		-- entry Finish(L:out Natural);
	 end Client;
end Client;
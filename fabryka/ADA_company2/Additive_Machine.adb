with Ada.Text_IO, declarations, buffers, MRam,Ada.Numerics.Discrete_Random; 
use Ada.Text_IO, declarations, buffers, MRam;
--============================================================
package body Additive_Machine is
--============================================================
	 task body Additive_Machine is
			Id:Natural;
			B:Button:=0;
			Disabled:Boolean;
			Output:Integer;
			MyStat:Integer:=0;
		begin
--============================================================
			accept Start(Id :in Natural) do
				Additive_Machine.Id:=Id;
				Put_Line("AMid= "&Natural'Image(Id));
				Disabled:=False;
			end Start;
--============================================================
			while not Finished loop 
				exit when SHUTorNOT=SHUTDOWN_BUTTON;
				select 
					
					accept Add( Id:in Natural; F:in Integer; I:in Integer; Y:in Character; Mode:in Integer) do
						MyStat:=1; -- machine is busy from now
						if Y='+' then
						Output:=F+I;
						elsif Y='-' then
						Output:=F-I;
						end if;
						delay 0.0;
					
					end Add;
				or
					accept GetAOut(Output:out Integer) do
						Output:=Additive_Machine.Output;
					end GetAOut;
				or
					
					accept Report(Id:in Natural; MyStat:out Integer) do
						MyStat:=Additive_Machine.MyStat; -- report my status
						-- Put_Line("AM["&Natural'Image(Id)&"]-status:"&Integer'Image(MyStat));
						-- null; 	--send status of this machine 0 / 1 / 2;
					end Report;
					exit when SHUTorNOT=SHUTDOWN_BUTTON;
				or accept PlanB do
					null;
				end PlanB;
				or delay 2.0;
				
				 accept Req do
					if declarations.MODE>0 then
					Put_Line("AMachine["&Natural'Image(Additive_Machine.Id)&"] is being over used.");
					end if;
					 requeue PlanB with abort;
				 end Req;
				end select;
			end loop;
--============================================================
			-- Put_Line("Additive_Machine waiting for finish.");
			-- accept Finish(Id:out Natural) do
				-- Id:=Additive_Machine.Id;
				-- -- !("MMid!end= "&Natural'Image(Id));
			-- end Finish;							
			 Put_Line("AMid!end="&Natural'Image(Id));
--============================================================
	end Additive_Machine;
--======================================================================Additive_Machine
end Additive_Machine;
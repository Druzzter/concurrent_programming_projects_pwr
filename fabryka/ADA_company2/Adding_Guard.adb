--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
with Ada.Text_IO,declarations, MRam, Repair;
use Ada.Text_IO,declarations, MRam, Repair;
package body Adding_Guard is
task body Adding_Guard is
	Length : Natural range 0..Q_Size2:=0;
			Id:Natural:=1;
			tNr:Natural;
			tF:Integer;
			tI:Integer;
			tY:Character;
			tM:Integer;
			Head,Tail :Q_Range2 := 1;
			Output:Integer:=0;
			Computer:array (Q_Range2) of MRam.ARam;
			NeedsRepair:Q_Range2:=1;
		begin 
		accept Start do
		Put_Line("Adding Computers' Guard started.");
		
		end Start;
		while not Finished loop 
				exit when SHUTorNOT=SHUTDOWN_BUTTON;
				select 
					 when Length < Q_Size2 =>
						accept Insert( Nr:in Natural;F:in Integer; I:in Integer; Y:in Character; Mode:in Integer) do
						Adding_Guard.tNr:=Nr;
						Adding_Guard.tF:=F;
						Adding_Guard.tI:=I;
						Adding_Guard.tY:=Y;
						Adding_Guard.tM:=Mode;
						if F<5 then
							Put_Line("AM needs repair!");
							NeedsRepair:=Tail;
							requeue GoAndFix;
						else 
							Computer(Tail).Add(F,I,Y);
							Put_Line("Worker["&Natural'Image(Nr)&"] computing task: "&Integer'Image(F)&Character'Image(Y)&Integer'Image(I));
						Tail:=Tail mod Q_Size2+1;
						Length:=Length+1;	
						end if;
						end Insert;
						
				or	when Length> 0 =>
						accept Remove(Id:in Natural;Output:out Integer) do
							Computer(Head).Del(Output);
							Put_Line("Worker["&Natural'Image(Id)&"] took: "&Integer'Image(Output));
							Head:=Head mod Q_Size2+1;
					Length:=Length-1;
						end Remove;
					exit when SHUTorNOT=SHUTDOWN_BUTTON;
				or 
						accept GoAndFix do
							Put_Line("Passing task and Repairing MMachine.");
							NeedsRepair:=1;
							requeue Pass;
						end GoAndFix;
				or 
						accept Pass do
							Put_Line("Passed task to new MMachine.");
							Computer(Tail).Add(Adding_Guard.tF,Adding_Guard.tI,Adding_Guard.tY);
							Tail:=Tail mod Q_Size1+1;
							Length:=Length+1;
							Put_Line("Worker["&Natural'Image(Adding_Guard.tNr)&"] computing task: "&Integer'Image(Adding_Guard.tF)&Character'Image(Adding_Guard.tY)&Integer'Image(Adding_Guard.tI));
						end Pass;
				or delay 3.0;
					Put_Line("Adding Computers' Guard noticed lack of addition in company.");
					exit;
				end select;
			end loop;				
			Put_Line("Adding Computers' Guard has left.");
	end Adding_Guard;
end Adding_Guard;
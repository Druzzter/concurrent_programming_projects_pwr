--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
with Ada.Text_IO, declarations, buffers; 
use Ada.Text_IO, declarations, buffers;

package body Magazyn is		
	 task body Magazyn is
			Q_Size:constant:=NoOfItem;
			subtype Q_Range is Positive range 1..Q_Size;
			Length : Natural range 0..Q_Size:=0;
			Id:Natural:=2;
			Head,Tail :Q_Range := 1;
		begin
			accept Start(Id :in Natural) do
				Magazyn.Id:=Id;
				Put_Line("Warehouse keeper came to work early today.");
			end Start;
			while not Finished loop 
				exit when SHUTorNOT=SHUTDOWN_BUTTON;
				select 
					when Length < Q_Size =>
						accept Insert(F:in Integer; Nr:in Natural) do
						if SHUTorNOT/=SHUTDOWN_BUTTON then 
							NewBuffer.Add(F);
							end if;
							if declarations.Mode >0 then
							Put_Line("WORKER[" & Natural'Image(Nr)&"] Selling product "&Integer'Image(F)&" to the Warehouse.");
							else
								null;
							end if;
						end Insert;
						exit when SHUTorNOT=SHUTDOWN_BUTTON;
						Tail:=Tail mod Q_Size+1;
						Length:=Length+1;
						-- Put_Line("Magazyn size["&Integer'Image(Length)&"]");
				or
					when Length> 0 =>
						accept Remove(F:out Integer; Nr:in Natural) do
							if SHUTorNOT/=SHUTDOWN_BUTTON then 
							NewBuffer.Del(F);
							end if;
							if declarations.Mode >0 then
							Put_Line("CLIENT["&Natural'Image(Nr)&"] bought "&Integer'Image(F)&" from the Warehouse.");
							else
								null;
							end if;
							delay DelayTimer*MultiAdder+MultiAdder*declarations.SPEED;
						end Remove;
						exit when SHUTorNOT=SHUTDOWN_BUTTON;
						Head:=Head mod Q_Size+1;
						Length:=Length-1;
						-- Put_Line("Magazyn size["&Integer'Image(Length)&"]");
				or delay 1.0;
					exit;
				end select;
				exit when SHUTorNOT=SHUTDOWN_BUTTON;
			end loop;
			-- accept Finish(Id:out Natural) do
				-- Id:=Magazyn.Id;
				 -- Put_Line("Magazyn id!end= "&Natural'Image(Id));
			-- end Finish;
	end Magazyn;
	 --======================================================================MAGAZYN
end Magazyn;
--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
with declarations;
package Repair is
task Repair is
		entry Start;
		-- entry Finish(Id: out Natural);		
		entry Fix(MNeedsRepair:in declarations.Q_Range1; ANeedsRepair:in declarations.Q_Range2);
	 end Repair;
end Repair;
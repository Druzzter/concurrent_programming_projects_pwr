--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
package Magazyn is
task  Magazyn is
		entry Start(Id:in Natural);
		-- entry Finish(Id:out Natural);
		entry Insert(F:in Integer; Nr :in Natural);
		entry Remove(F:out Integer; Nr:in Natural);
	 end Magazyn;
end Magazyn;
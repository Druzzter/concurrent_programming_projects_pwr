--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
with Ada.Text_IO,declarations, MRam;
use Ada.Text_IO,declarations, MRam;
package body Computing_Guard is
task body Computing_Guard is
	Length : Natural range 0..Q_Size1:=0;
			Id:Natural:=1;
			tNr:Natural;
			tF:Integer;
			tI:Integer;
			tY:Character;
			tM:Integer;
			Head,Tail :Q_Range1 := 1;
			Head2,Tail2:Q_Range1 :=1;
			Output:Integer:=0;
			Computer:array (Q_Range1) of MRam.MRam;
			NeedHelp:array (Q_Range1) of Boolean;
			NeedsRepair:Q_Range1:=1;
		begin 
		accept Start do
		Put_Line("Multiplication Computers' Guard came to work.");
		for I in Q_Range1 loop
			NeedHelp(I):=False;
		end loop;
		end Start;
		while not Finished loop 
				exit when SHUTorNOT=SHUTDOWN_BUTTON;
				select 
					 when Length < Q_Size1 =>
						accept Insert( Nr:in Natural;F:in Integer; I:in Integer; Y:in Character; Mode:in Integer) do
							Computing_Guard.tNr:=Nr;
							Computing_Guard.tF:=F;
							Computing_Guard.tI:=I;
							Computing_Guard.tY:=Y;
							Computing_Guard.tM:=Mode;
						-- Put_Line("1a.. ... ...M");	
							if F<5 then
							Put_Line("I NEED REP!");
								NeedsRepair:=Tail;
								requeue GoAndFix;
							else
							Computer(Tail).Add(F,I,Y);
							-- Put_Line("1b.. ... ...M");
							Put_Line("Worker["&Natural'Image(Nr)&"] computing task: "&Integer'Image(F)&Character'Image(Y)&Integer'Image(I));
							Tail:=Tail mod Q_Size1+1;
						Length:=Length+1;
							end if;
							-- NeedHelp(Tail):=True;
						end Insert;
				or	when Length > 0 =>
						accept Remove(Id:in Natural;Output:out Integer) do
						-- Put_Line("2a.. ... ... M");
							Computer(Head).Del(Output);
							-- Put_Line("2b.. ... ...M");
							Put_Line("Worker["&Natural'Image(Id)&"] took: "&Integer'Image(Output));
							-- NeedHelp(Head):=False;
						end Remove;
					exit when SHUTorNOT=SHUTDOWN_BUTTON;
					Head:=Head mod Q_Size1+1;
					Length:=Length-1;
				or
					accept Ping do
					-- Put_Line("Helping");
						Computer(Tail).Help;
						-- Put_Line("Helped? - leave LIKE.");
					end Ping;
				or 
						accept GoAndFix do
							Put_Line("Passing task and Repairing MMachine.");
							NeedsRepair:=1;
							requeue Pass;
						end GoAndFix;
				or 
						accept Pass do
							Put_Line("Passed task to new MMachine.");
							Computer(Tail).Add(Computing_Guard.tF,Computing_Guard.tI,Computing_Guard.tY);
							Tail:=Tail mod Q_Size1+1;
							Length:=Length+1;
							Put_Line("Worker["&Natural'Image(Computing_Guard.tNr)&"] computing task: "&Integer'Image(Computing_Guard.tF)&Character'Image(Computing_Guard.tY)&Integer'Image(Computing_Guard.tI));
						end Pass;
				or delay 3.0;
					Put_Line("Multiplication Computers' Guard has noticed company lack of multiplication!");
					exit;
				end select;
				delay 0.001; -- TASK SWAPING TRICK
			end loop;				
			Put_Line("Multiplication Computers' Guard went home.");
	end Computing_Guard;
end Computing_Guard;
--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
with Ada.Text_IO, declarations, buffers, Observer, Lista_Guard, Magazyn, Boss; 
use Ada.Text_IO, declarations, buffers, Observer, Lista_Guard, Magazyn, Boss;

package body Client is
	 task body Client is
			L: Natural;
			I: Integer:=0;
		begin 
			accept Start(L:in Natural) do
				Client.L:=L;
		    Put_line("Client["& Natural'Image(L)&"] - started to sniff around for new programs.");
			end Start;
		while not Finished loop
				exit when I=NoOfItem or SHUTorNOT=SHUTDOWN_BUTTON;
				if SHUTorNOT/=SHUTDOWN_BUTTON then 
				I:=I+1;
				Magazyn.Magazyn.Remove(I,L);
				else exit;
				end if;
				delay DelayTimer*declarations.SPEED;
			end loop;
			-- Put_Line("Client waits for end!"&Natural'Image(L));
			-- accept Finish(L:out Natural) do
				-- L:=Client.L;
				 -- Put_Line("Cid!end= "&Natural'Image(L));
			-- end Finish;
	 end Client;
--======================================================================CLIeNT
end Client;
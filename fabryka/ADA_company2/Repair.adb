--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
with Ada.Text_IO, declarations, buffers, Lista_Guard, RandGen, Ada.Numerics.Discrete_Random, taskdefinitions; 
use Ada.Text_IO, declarations, buffers, Lista_Guard, RandGen, taskdefinitions;
package body Repair is

	 task body Repair is
	 Id:Natural:=4;
	 Need:Natural;
   package Rand_Int is new Ada.Numerics.Discrete_Random(Integer);
   seed : Rand_Int.Generator;
   Num : Integer;
	 begin
		
			accept Start do
				Put_Line("RepairMan came to work."&Natural'Image(Id));
				Repair.Need:=0;
			end Start;
			while not Finished loop
				exit when SHUTorNOT=SHUTDOWN_BUTTON;
				exit when Finished=True;
				if SHUTorNOT/=SHUTDOWN_BUTTON then 
						Rand_Int.Reset(seed);
						Num := Rand_Int.Random(seed);
						Num:=Num mod 1337;
							if Num>0 and Num<334 then 	 null;
					  elsif Num>333 and Num<668 then null;
					  elsif Num>667 and Num<999 then null;
						else null;
						end if;
						Num:=Num mod 1337;
				 else exit;
				end if;
				select 
							accept Fix(MNeedsRepair:in Q_Range1; ANeedsRepair:in Q_Range2) do
								null;
							end Fix;
					end select;
				 delay DelayTimer*declarations.SPEED;
			end loop;
			
			-- accept Finish(Id :out Natural) do
				-- Id:=Repair.Id;
				 Put_Line("RepairMan went home.");
			-- end Finish;
	 end Repair;
	 --======================================================================BOSS
end Repair;
with Ada.Text_IO, declarations, buffers; 
use Ada.Text_IO, declarations, buffers;
--==================================================================OBSERVER
package body Observer is
	 task body Observer is
			Id : Natural:=3;
      Answer : Character;
			Prompt :String := "Your answer (Y/N): ";
   begin
			accept Start(Id :in Natural) do
				Observer.Id:=Id;
				Put_Line("Oid= "&Natural'Image(Id));
			end Start;
      Ada.Text_IO.Put_Line(Prompt);
      loop
         Ada.Text_IO.Get_Immediate (Answer);
         case Answer is
            when 'Y'|'y' =>
							Put_Line("--{~~~~~~}--");
							Lista.Print(1);
							NewBuffer.Print(1);
							Put_Line("List size => "&Natural'Image(NoOfItem));
							Put_Line("Magazyn size => "&Natural'Image(NoOfItem2));
							Put_Line("# of Workers => "&Integer'Image(NoOfWorkers));
							Put_Line("# of Clients => "&Integer'Image(NoOfClients));
							Put_Line("Global speed => "&Duration'Image(SPEED));
							Put_Line("--{~~~~~~}--");
            when 'N'|'n' => 
								SHUTorNOT:=SHUTDOWN_BUTTON;
								Put_Line(">>>>>>>>>>>>>>>>>>>>>>>>>Shutdown<<<<<<<<<<<<<<<<<<<<<<<<<<<");
						when '0' =>
							declarations.MODE:=0;
						when '1' =>
							declarations.MODE:=1;
						when '-' =>
							declarations.SPEED:=declarations.SPEED-1.0;
						when '=' =>
							declarations.SPEED:=declarations.SPEED+1.0;
            when others  => 
						Put_Line("...");
         end case;
				 exit when SHUTorNOT=SHUTDOWN_BUTTON;
      end loop;
			Put_Line("Observer waiting for Quit");
			accept Finish(Id :out Natural) do
				Id:=Observer.Id;
				Put_Line("Oid!end= "&Natural'Image(Id));
			end Finish;
			Put_Line("Observer RAGE-QUIT!");
   end Observer;
end Observer;
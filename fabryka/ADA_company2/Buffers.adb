with Ada.Text_IO;
use Ada.Text_IO;
package body Buffers is
--============================================================
	 protected body NewBuffer is
			entry Add(I:in Integer) when Count<Index2'Last is
			begin 
				B(In_P):=I;
				Count:=Count+1;
				In_P:=In_P+1;
			end Add;
			
			entry Del(I: out Integer) when Count>0 is
			begin 
				I:=B(Out_P);
				Count:=Count-1;
				Out_P:=Out_P+1;
			end Del;
			
			procedure Print(I:in Integer) is
			begin 
				Put_Line("Warehouse stock:"&Index2'Image(Count));
			end Print;
		end NewBuffer;

--============================================================
	 protected body Lista is
			entry Add(I:in Integer;J:in Integer;Y:in Character) when Count<Index'Last is
			begin 
				B(In_P).x:=I;
				B(In_P).y:=J;
				B(In_P).z:=Y;
				Count:=Count+1;
				In_P:=In_P+1;
			end Add;
			entry Del(I: out Integer; J:out Integer; Y:out Character) when Count>0 is
			begin 
				I:=B(Out_P).x;
				J:=B(Out_P).y;
				Y:=B(Out_P).z;
				Count:=Count-1;
				Out_P:=Out_P+1;
			end Del;
			procedure Print(I:in Integer) is
			begin 
				Put_Line("List stock:"&Index'Image(Count));
			end Print;
		end Lista;
--============================================================
end Buffers;
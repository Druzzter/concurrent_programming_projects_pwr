--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
-- USER CAN MODULATE IN REAL TIME THE PROGRAM FLOW
	--by pressing keys:
	-- VERBOSE MODE = 1
	-- SILENT  MODE = 0
	-- SPEED UP = '='
	-- SPEED DOWN = '-'
	-- MORE INFO    = y   or   Y
	-- TURN OFF MODULATOR = n    or    Natural
	-- TURN OFF PROGRAM   = ctrl + c
package declarations is
   NoOfItem		:constant Natural:=100;
	 ItemsMade 	: Natural:=0;
	 NoOfItem2	:constant Natural:=100;
	 DelayTimer	:constant Duration:=0.1;
	 MultiAdder :constant Duration:=0.1;
	 StandardSpeed:constant Duration:=0.0;
	 SPEED			:Duration:=2.0; --MODULATE FOR SPEED
	 NoOfWorkers:constant Integer:=2;
	 NoOfHWorker1s:constant Integer:=20;
	 Pwork 			:Natural:=1;
	 PworkI 		:Integer:=1;
	 NoOfClients :constant Integer:=5;
	 NoOfMMachs	 :constant Integer:=3;
	 NoOfAMachs :constant Integer:=3;
	 MODE: Integer:=0;
	 IdLista:Natural:=1;
	 IdMagazyn:Natural:=2;
	 IdObserver:Natural:=3;
	 IdBoss:Natural:=4;
	 IdMM:Natural:=5;
	 IdAM:Natural:=6;
	 IdAMG:Natural:=7;
	 SHUTDOWN_BUTTON :constant Character:='q';
	 SHUTorNOT : Character:='.';
	 INatural :Natural:=0;
	 Waste:Natural:=0;
	 Answer : Character:='x';
	 Finished : Boolean := False;
	 ProblemLevel: Natural :=1;
	 Added:Boolean:=False;
	 HelpingBossOption:Boolean:=False;
	 Probability:Natural:=10000000;
	 -- type Character is ('+','-','*');
	 type zadanie is record 
			x :Integer;
			y :Integer;
			z :Character;
			output:Integer;
	 end record;
	 subtype Button is Natural range 0..2;
   type Index 					is mod NoOfItem;
	 type Index2					is mod NoOfItem2;
	 type Index3 					is mod NoOfMMachs;
	 type Index4 					is mod NoOfAMachs;
	 -- type MRamCells 			is array(Index3) of Integer;
   type BufferArray 		is array(Index) of Integer;
	 type NewBufferArray 	is array(Index2) of Integer;
	 type ListaZadan			is array(Index) of zadanie;
	 type ListaZadan2 		is array(Index3) of zadanie;
	 type ListaZadan3 		is array(Index4) of zadanie;
	 -- MR : MRam.MRam;
	 -- MRarr: array(1..NoOfMMachs) of MRam.MRam;
	 
		Q_Size1:constant:=NoOfAMachs;
		subtype Q_Range1 is Positive range 1..Q_Size1;
		StatusAM: array (Q_Range1) of Integer;
	 
	 Q_Size2:constant:=NoOfMMachs;
		subtype Q_Range2 is Positive range 1..Q_Size2;
		StatusMM: array (Q_Range2) of Integer;
	 
	 
end declarations;
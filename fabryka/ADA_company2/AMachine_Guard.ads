package AMachine_Guard is
task  AMachine_Guard is
		entry Start(Id:in Natural);
		-- entry Finish(Id:out Natural);
		entry Insert( Nr:in Natural; F:in Integer; I:in Integer; Y:in Character; Mode:in Integer);
		entry Remove(Id:in Natural; Output:out Integer);
end AMachine_Guard;
end AMachine_Guard;
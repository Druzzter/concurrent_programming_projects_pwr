package RandGen is
   function generate_random_number ( n: in Positive) return Positive;
end RandGen;
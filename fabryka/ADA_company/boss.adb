with Ada.Text_IO, declarations, buffers; 
use Ada.Text_IO, declarations, buffers;

procedure boss is
		 
		 task Boss;
		 task body Boss is
		 begin
				for N in 1..NoOfItem loop
					 Put_Line("Product concept added" & Integer'Image(N));
					 Buffer.Append(N);
				end loop;
		 end Boss;
begin
	null;
end boss;
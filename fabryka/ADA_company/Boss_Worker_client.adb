with Ada.Text_IO, declarations, buffers; 
use Ada.Text_IO, declarations, buffers;

procedure Boss_Worker_Client is
--============================================================
		MODE:Integer:=0;
		SPEED:Duration:=declarations.SPEED;
--============================================================
	 task  Observer is
		entry Start(Id :in Natural);
		entry Finish(Id:out Natural);
	 end Observer;
	 task body Observer is
			Id : Natural:=3;
      Answer : Character;
			Prompt :String := "Your answer (Y/N): ";
   begin
			accept Start(Id :in Natural) do
				Observer.Id:=Id;
				Put_Line("Oid= "&Natural'Image(Id));
			end Start;
      Ada.Text_IO.Put_Line(Prompt);
      loop
         Ada.Text_IO.Get_Immediate (Answer);
         case Answer is
            when 'Y'|'y' =>
							Put_Line("--{~~~~~~}--");
							Buffer.Print(1);
							NewBuffer.Print(1);
							Put_Line("List size => "&Natural'Image(NoOfItem));
							Put_Line("Magazyn size => "&Natural'Image(NoOfItem2));
							Put_Line("# of Workers => "&Integer'Image(NoOfWorkers));
							Put_Line("# of Clients => "&Integer'Image(NoOfClients));
							Put_Line("Global speed => "&Duration'Image(SPEED));
							Put_Line("--{~~~~~~}--");
            when 'N'|'n' => 
								exit;
						when '0' =>
							MODE:=0;
						when '1' =>
							MODE:=1;
						when '-' =>
							SPEED:=SPEED-1.0;
						when '=' =>
							SPEED:=SPEED+1.0;
            when others  => 
						Put_Line("...");
         end case;
      end loop;
			accept Finish(Id :out Natural) do
				Id:=Observer.Id;
				Put_Line("Oid!end= "&Natural'Image(Id));
			end Finish;
   end Observer;
--==================================================================OBSERVER
	 task  Lista_Guard is
	  entry Start(Id:in Natural);
		entry Finish(Id:out Natural);
		entry Insert(F:in Integer; I:in Integer; Y:in Character; Nr:in Natural; Mode:in Integer);
		entry Remove(F:out Integer;I:out Integer; Y:out Character; Nr:in Natural; Mode:in Integer);
	 end Lista_Guard;
		
	 task body Lista_Guard is
			Q_Size:constant:=NoOfItem;
			subtype Q_Range is Positive range 1..Q_Size;
			Length : Natural range 0..Q_Size:=0;
			Id:Natural:=1;
			Head,Tail :Q_Range := 1;
		begin
			accept Start(Id :in Natural) do
				Lista_Guard.Id:=Id;
				Put_Line("Lid= "&Natural'Image(Id));
			end Start;
			loop 
				select 
					when Length < Q_Size =>
						accept Insert(F:in Integer; I:in Integer; Y:in Character; Nr:in Natural; Mode:in Integer) do
							Lista.Add(F,I,Y);
							if Mode >0 then
							Put_Line("BOSS["&Natural'Image(Nr)&"] added task {"&Integer'Image(F)&Character'Image(Y)&Integer'Image(I)&"} to the list.");
							else
								null;
							end if;
						end Insert;
						Tail:=Tail mod Q_Size+1;
						Length:=Length+1;
				or
					when Length> 0 =>
						accept Remove(F:out Integer; I:out Integer; Y:out Character; Nr :in Natural; Mode:in Integer) do
							Lista.Del(F,I,Y);
							if Mode>0 then
							Put_Line("WORKER["&Natural'Image(Nr)&"] took task "&Integer'Image(F)&Character'Image(Y)&Integer'Image(I)&" from the list.");
							else
								null;
							end if;
						end Remove;
						Head:=Head mod Q_Size+1;
						Length:=Length-1;
				end select;
			end loop;
			accept Finish(Id:out Natural) do
				Id:=Lista_Guard.Id;
				Put_Line("Lid!end= "&Natural'Image(Id));
			end Finish;
	end Lista_Guard;
	 --======================================================================LISTA
	task  Cyclic_Buffer_Task_Type is
	  entry Start(Id:in Natural);
		entry Finish(Id:out Natural);
		entry Insert(F:in Integer; Nr: in Natural);
		entry Remove(F:out Integer; Nr: in Natural);
	 end Cyclic_Buffer_Task_Type;
		
	 task body Cyclic_Buffer_Task_Type is
			Q_Size:constant:=NoOfItem;
			subtype Q_Range is Positive range 1..Q_Size;
			Length : Natural range 0..Q_Size:=0;
			Id:Natural:=1;
			Head,Tail :Q_Range := 1;
		begin
			accept Start(Id :in Natural) do
				Cyclic_Buffer_Task_Type.Id:=Id;
				Put_Line("Lid= "&Natural'Image(Id));
			end Start;
			loop 
				select 
					when Length < Q_Size =>
						accept Insert(F:in Integer; Nr:in Natural) do
							Buffer.Append(F);
							Put_Line("BOSS["&Natural'Image(Nr)&"] added task "&Integer'Image(F)&" to the list.");
						end Insert;
						Tail:=Tail mod Q_Size+1;
						Length:=Length+1;
						-- Put_Line("List size["&Integer'Image(Length)&"]");
				or
					when Length> 0 =>
						accept Remove(F:out Integer; Nr :in Natural) do
							Buffer.Take(F);
							Put_Line("WORKER["&Natural'Image(Nr)&"] took task "&Integer'Image(F)&" from the list.");
							
						end Remove;
						Head:=Head mod Q_Size+1;
						Length:=Length-1;
						-- Put_Line("List size["&Integer'Image(Length)&"]");
				end select;
			end loop;
			accept Finish(Id:out Natural) do
				Id:=Cyclic_Buffer_Task_Type.Id;
				Put_Line("Lid!end= "&Natural'Image(Id));
			end Finish;
	end Cyclic_Buffer_Task_Type;
	 --======================================================================LISTA
	
	 task  Magazyn is
		entry Start(Id:in Natural);
		entry Finish(Id:out Natural);
		entry Insert(F:in Integer; Nr :in Natural);
		entry Remove(F:out Integer; Nr:in Natural);
	 end Magazyn;
		
	 task body Magazyn is
			Q_Size:constant:=NoOfItem;
			subtype Q_Range is Positive range 1..Q_Size;
			Length : Natural range 0..Q_Size:=0;
			Id:Natural:=2;
			Head,Tail :Q_Range := 1;
		begin
			accept Start(Id :in Natural) do
				Magazyn.Id:=Id;
				Put_Line("Mid= "&Natural'Image(Id));
			end Start;
			loop 
				select 
					when Length < Q_Size =>
						accept Insert(F:in Integer; Nr:in Natural) do
							NewBuffer.Add(F);
							if Mode >0 then
							Put_Line("WORKER[" & Natural'Image(Nr)&"] Selling product "&Integer'Image(F)&" to the magazyn.");
							else
								null;
							end if;
						end Insert;
						Tail:=Tail mod Q_Size+1;
						Length:=Length+1;
						-- Put_Line("Magazyn size["&Integer'Image(Length)&"]");
				or
					when Length> 0 =>
						accept Remove(F:out Integer; Nr:in Natural) do
							NewBuffer.Del(F);
							if Mode >0 then
							Put_Line("CLIENT["&Natural'Image(Nr)&"] bought "&Integer'Image(F)&" from the magazyn.");
							else
								null;
							end if;
							delay DelayTimer*MultiAdder+MultiAdder*SPEED;
						end Remove;
						Head:=Head mod Q_Size+1;
						Length:=Length-1;
						-- Put_Line("Magazyn size["&Integer'Image(Length)&"]");
				end select;
			end loop;
			accept Finish(Id:out Natural) do
				Id:=Magazyn.Id;
				Put_Line("Mid!end= "&Natural'Image(Id));
			end Finish;
	end Magazyn;
	 --======================================================================MAGAZYN
	 
	 task Boss is
		entry Start(Id:in Natural);
		entry Finish(Id: out Natural);
	 end Boss;
	 task body Boss is
	 Id:Natural:=4;
	 begin
		
			accept Start(Id :in Natural) do
				Boss.Id:=Id;
				Put_Line("Bid= "&Natural'Image(Id));
			end Start;
			for N in 1..NoOfItem loop
				 --Buffer.Append(N);
				 -- Cyclic_Buffer_Task_Type.Insert(N,Id);
				 Lista_Guard.Insert(N,N,'+',Id,MODE);
				 delay DelayTimer*SPEED;
			end loop;
			
			accept Finish(Id :out Natural) do
				Id:=Boss.Id;
				Put_Line("Lid!end= "&Natural'Image(Id));
			end Finish;
	 end Boss;
	 --======================================================================BOSS
   task type Worker is
		entry Start(F:in Natural);
		entry Finish(F: out Natural);
	 end Worker;
   task body Worker is
			Farg:Integer;
			Sarg:Integer;
			Op:Character;
			Output:Integer;
      F:Natural;
      I:Natural:=0;
   begin
		accept Start(F:in Natural) do
				Worker.F:=F;
		    Put_line("Wid= "& Natural'Image(F));
		end Start;
      loop
				 I:=I+1;
				 -- Cyclic_Buffer_Task_Type.Remove(I,F);
				 Lista_Guard.Remove(Farg,Sarg,Op,F,MODE);
						if Op = '+' then
						Worker.Output:=Farg + Sarg;
						elsif Op = '-' then
						Worker.Output:=Farg-Sarg;
						elsif Op= '*' then
						Worker.Output:=Farg*Sarg;
						else
							Worker.Output:=0;
						end if;
				 Magazyn.Insert(Output,F);
        delay DelayTimer+MultiAdder*SPEED;
						null;
					exit when I=NoOfItem;
      end loop;
			accept Finish(F:out Natural) do
				F:=Worker.F;
				Put_Line("Wid!end= "&Natural'Image(F));
			end Finish;
   end Worker;
	 --======================================================================WORKER
	 task type Client is
		entry Start(L:in Natural);
		entry Finish(L:out Natural);
	 end Client;
	 task body Client is
			L: Natural;
			I: Integer:=0;
		begin 
			accept Start(L:in Natural) do
				Client.L:=L;
		    Put_line("Cid= "& Natural'Image(L));
			end Start;
			loop
				I:=I+1;
				Magazyn.Remove(I,L);
				exit when I=NoOfItem;
				delay DelayTimer*SPEED;
			end loop;
			accept Finish(L:out Natural) do
				L:=Client.L;
				Put_Line("Cid!end= "&Natural'Image(L));
			end Finish;
	 end Client;
--======================================================================CLIeNT
	-- subtype wk is Worker;
	-- subtype cl is Client;
	 Workers : array(1..NoOfWorkers) of Worker;
	 Clients : array(1..NoOfClients) of Client;
	 
begin
	 -- Cyclic_Buffer_Task_Type.Start(IdLista);
	 Observer.Start(IdObserver);
	 Magazyn.Start(IdMagazyn);
	 Lista_Guard.Start(IdLista);
	 Boss.Start(IdBoss);
	 for I in 1..NoOfWorkers loop 
         Workers(I).Start(I); -- distribute work between workers
	 end loop;
	 for I in 1..NoOfClients loop
				 Clients(I).Start(I);
	 end loop;
--COMPANY IS WORKING FROM NOW ON!

	 
	-- for I in 1..NoOfWorkers loop
	-- Workers(I).Finish(I); --end of workers
	-- end loop;
	-- for I in 1..NoOfClients loop
	-- Clients(I).Finish(I);
	-- end loop;
end Boss_Worker_Client;


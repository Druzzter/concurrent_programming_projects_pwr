
package declarations is
	-- USER CAN MODULATE IN REAL TIME THE PROGRAM FLOW
	--by pressing keys:
	-- VERBOSE MODE = 1
	-- SILENT  MODE = 0
	-- SPEED UP = '='
	-- SPEED DOWN = '-'
	-- MORE INFO    = y   or   Y
	-- TURN OFF MODULATOR = n    or    Natural
	-- TURN OFF PROGRAM   = ctrl + c
   NoOfItem		:constant Natural:=100;
	 NoOfItem2	:constant Natural:=100;
	 DelayTimer	:constant Duration:=0.1;
	 MultiAdder :constant Duration:=0.1;
	 SPEED			:constant Duration:=2.0; --MODULATE FOR SPEED
	 NoOfWorkers:constant Integer:=10;
	 NoOfClients :constant Integer:=5;
	 MODE: Integer:=0;
	 IdLista:Natural:=1;
	 IdMagazyn:Natural:=2;
	 IdObserver:Natural:=3;
	 IdBoss:Natural:=4;
	 
	 -- type Character is ('+','-','*');
	 type zadanie is record 
			x :Integer;
			y :Integer;
			z :Character;
	 end record;
	 
   type Index 					is mod NoOfItem;
	 type Index2					is mod NoOfItem2;
   type BufferArray 		is array(Index) of Integer;
	 type NewBufferArray 	is array(Index2) of Integer;
	 type ListaZadan			is array(Index) of zadanie;
end declarations;
with declarations;
use declarations;

package Buffers is
--============================================================
		protected NewBuffer is
			entry Add(I: in Integer);
			entry Del(I: out Integer);
			procedure Print(I: in Integer);
		private
			B: NewBufferArray;
			In_P,Out_P,Count:Index2:=0;
			Empty :Boolean :=True;
	 end NewBuffer;
--============================================================MAGAZYN
   protected  Buffer is
      entry Append (I: in Integer);
      entry Take (I: out Integer);
			procedure Print(I: in Integer);
   private
      B: BufferArray; --Chroniony zasob
      In_P,Out_P, Count:Index:=0;
			Empty :Boolean :=True;
   end Buffer;
--=============================================================LISTA
	 protected Lista is
			entry Add(I:in Integer; J:in Integer; Y:in Character);
			entry Del(I:out Integer; J:out Integer; Y:out Character);
			procedure Print(I:in Integer);
	 private
			B: ListaZadan;
			In_P, Out_P, Count:Index:=0;
			Empty :Boolean :=True;
	 end Lista;
--============================================================LISTA
end Buffers;

package declarations is
	
   NoOfItem		:constant Natural:=100;
	 DelayTimer	:constant Duration:=1.0;
	 MultiAdder :constant Duration:=1.2;
	 SPEED			:constant Duration:=1.0; --MODULATE FOR SPEED
	 
	 type zadanie is record 
			x :Integer;
			y :Integer;
			z :String(1..2);
	 end record;
	 
   type Index 					is mod NoOfItem;
   type BufferArray 		is array(Index) of zadanie;
	 type NewBufferArray 	is array(Index) of Integer;

end declarations;
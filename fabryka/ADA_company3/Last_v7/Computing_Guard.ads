--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
with declarations;
package Computing_Guard is
task type Computing_Guard is
		entry Start;
		-- entry Finish(Id:out Natural);
		entry Insert( Nr:in Natural; F:in Integer; I:in Integer; Y:in Character; Mode:in Integer);
		entry Remove(Id:in Natural; Output:out Integer);
		entry Ping;
		entry GoAndFix;
		entry Pass;
end Computing_Guard;
end Computing_Guard;

with declarations, Multiply_Machine,Additive_Machine;
use declarations, Multiply_Machine,Additive_Machine;
package taskdefinitions is
	 AMachines:array(1..NoOfAMachs) of Additive_Machine.Additive_Machine;
	 MMachines: array(1..NoOfMMachs)  of Multiply_Machine.Multiply_Machine;
end taskdefinitions;
	 
with Ada.Text_IO, declarations, taskdefinitions;
use Ada.Text_IO, declarations, taskdefinitions;

package body MMachine_Guard is
	 task body MMachine_Guard is
			-- Q_Size:constant:=NoOfAMachs;
			-- subtype Q_Range is Positive range 1..Q_Size;
			Length : Natural range 0..Q_Size2:=0;
			Id:Natural:=1;
			TaskId:Natural;
			Head,Tail :Q_Range2 := 1;
			Output:Integer:=0;
			-- StatusMM: array (Q_Range) of Integer;
		begin
			accept Start(Id :in Natural) do
				MMachine_Guard.Id:=Id;
				Put_Line("MMGid= "&Natural'Image(Id));
				for I in 1..NoOfMMachs loop 						-- start Multiply_Machines
					MMachines(I).Start(I);
				end loop;
				for J in 1..Q_Size2 loop
					StatusMM(J):=0;
					Put_Line("MMachine"&Integer'Image(J)&" ready!");
				end loop;
				Put_Line("All MMachines are ready!");
			end Start;
			while not Finished loop 
				exit when SHUTorNOT=SHUTDOWN_BUTTON;
				select 
					 when Length < Q_Size2 =>
						accept Insert( Nr:in Natural;F:in Integer; I:in Integer; Y:in Character; Mode:in Integer) do
						declarations.ProblemLevel:=declarations.ProblemLevel+1;
						if SHUTorNOT/=SHUTDOWN_BUTTON then 
						MMachine_Guard.TaskId:=Nr;
						Put_Line("MMG directed task:"&Natural'Image(Nr)&"=> MMachine["&"]");
							for J in 1..Q_Size2 loop
								 -- taskdefinitions.MMachines(J).Report(Nr, StatusMM(J)); --check status of machine!
								 Put_Line("MM["&Natural'Image(J)&"]-status:"&Integer'Image(StatusMM(J)));
								if StatusMM(J)=0 then
									StatusMM(J):=1; -- while machine is making math, it is busy
									if  declarations.MODE>0 then
									Put_Line("{");
									end if;
									taskdefinitions.MMachines(J).Multiply(Nr,F,I,Y, Mode); -- NA SZTYWNO DO POPRAWY!!!!!!!!!!!!!!!!!!!!!!!
									taskdefinitions.MMachines(J).GetMOut(MMachine_Guard.Output);
									if  declarations.MODE>0 then
									Put_Line("}");
									end if;
									StatusMM(J):=0; -- machine done its job - its free
									exit;
								elsif StatusMM(J)=1 then
									
									StatusMM(J):=2;
									null; -- Add machine is busy right now
								elsif StatusMM(J)=2 then
								
									StatusMM(J):=0;
									null; -- put here repair man!
								end if;
							end loop;
						end if;
						if Mode >0 then
							Put_Line("MM>Worker["&Natural'Image(Nr)&"] added task {"&Integer'Image(F)&Character'Image(Y)&Integer'Image(I)&"} for compute.");
						else
							null;
						end if;
						end Insert;
						exit when SHUTorNOT=SHUTDOWN_BUTTON;
						Tail:=Tail mod Q_Size2+1;
						Length:=Length+1;
				or
					when Length> 0 =>
						accept Remove(Id:in Natural;Output:out Integer) do
						if SHUTorNOT/=SHUTDOWN_BUTTON and declarations.MODE>0 then 
							Put_Line("MM>Worker["&Natural'Image(Id)&"]Removed his:{"&Integer'Image(MMachine_Guard.Output)&"} from computing");
						end if;
						Output:=MMachine_Guard.Output;
						declarations.ProblemLevel:=declarations.ProblemLevel-1;
					end Remove;
					exit when SHUTorNOT=SHUTDOWN_BUTTON;
					Head:=Head mod Q_Size2+1;
					Length:=Length-1;
				or delay 10.0;
					if declarations.MODE>0 then
						Put_Line("MMG got stuck and waits.");
					end if;
					for J in 1..Q_Size2 loop
					taskdefinitions.MMachines(J).Help(MMachine_Guard.Id);
					end loop;
				end select;
			end loop;
			-- Put_Line("ListGuard waiting for finish.");
			-- accept Finish(Id:out Natural) do
				-- Id:=MMachine_Guard.Id;
				-- -- !("Lid!end= "&Natural'Image(Id));
			-- end Finish;							
			 Put_Line("MMGid!end="&Natural'Image(Id));
	end MMachine_Guard;
	 --======================================================================LISTA
end MMachine_Guard;
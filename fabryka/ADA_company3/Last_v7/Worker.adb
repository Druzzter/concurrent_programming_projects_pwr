--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI

with Ada.Text_IO, declarations, buffers, Observer, Lista_Guard, Magazyn, Boss, Multiply_Machine, Additive_Machine, taskdefinitions,Ada.Numerics.Discrete_Random, MRam, Computing_Guard,Adding_Guard; 
use Ada.Text_IO, declarations, buffers, Observer, Lista_Guard, Magazyn, Boss, Multiply_Machine, Additive_Machine, taskdefinitions, MRam, Computing_Guard,Adding_Guard;
--============================================================
package body Worker is
  task body Worker1 is
			Farg:Integer;
			Sarg:Integer;
			Op:Character;
			Output:Integer;
      Id:Natural;
      I:Natural:=0;
			HelpingId:Natural:=0;
			package Rand_Int is new Ada.Numerics.Discrete_Random(Integer);
			seed : Rand_Int.Generator;
			ProductsMade:Natural;
			MyTarget:Natural;
			-- Num : Integer;
			-- Num2: Integer;
			
  begin
--============================================================
		accept Start(Id:in Natural) do
				Worker1.Id:=Id;
		    Put_line("Worker["& Natural'Image(Id)&"] - programmer came to work.");
				ProductsMade:=0;
				MyTarget:=NoOfWorkers/NoOfItem;
		end Start;
--============================================================
-- Put_Line(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Entering loop"&Natural'Image(Worker1.Id));
		while not Finished loop
				exit when SHUTorNOT=SHUTDOWN_BUTTON;
				 if SHUTorNOT/=SHUTDOWN_BUTTON then 
				 --bEFORE TAKING SOME NEW TASK TO DO HELP OTHERS WITH BUTTONS!
				 Put_Line("Free worker["&Natural'Image(Worker1.Id)&"] - tries to help someone.");
				    -- Computing_Guard.Computing_Guard.Ping; -- works only when FIRST Operation is always *
				 Lista_Guard.Lista_Guard.Remove(Worker1.Farg,Worker1.Sarg,Worker1.Op,Worker1.Id,declarations.MODE);
				 Put_Line("Worker["&Natural'Image(Id)&"]removed from List"&Integer'Image(Farg)&Character'Image(Op)&Integer'Image(Sarg));
				 else Put_Line("Something strange occured with Worker["&Natural'Image(Worker1.Id)&"]"); exit;
				 end if; --now try to do your own job.
						if Worker1.Op = '*' then
								-- Put_Line("Need to MULT");
								Computing_Guard.Computing_Guard.Ping;
								-- Put_Line("1... ... ...M");	
								Computing_Guard.Computing_Guard.Insert( Worker1.Id, Worker1.Farg, Worker1.Sarg, Worker1.Op, declarations.MODE );
								-- Put_Line("2... ... ...M");	
								Computing_Guard.Computing_Guard.Remove( Worker1.Id, Worker1.Output );
								-- Put_Line("3... ... ...M");
							-- AMachine_Guard.AMachine_Guard.Insert(Id,Farg,Sarg,Op,declarations.MODE);
							-- AMachine_Guard.AMachine_Guard.Remove(Id,Output);
						elsif Worker1.Op /= '*' then
								-- Put_Line("Need to ADD");
								Computing_Guard.Computing_Guard.Ping;
								-- Put_Line("1... ... ... A");
								Adding_Guard.Adding_Guard.Insert( Worker1.Id, Worker1.Farg, Worker1.Sarg, Worker1.Op, declarations.MODE );
								-- Put_Line("2... ... ... A");
								Adding_Guard.Adding_Guard.Remove( Worker1.Id, Worker1.Output );
								-- Put_Line("3... ... ... A");
							-- MMachine_Guard.MMachine_Guard.Insert(Id,Farg,Sarg,Op,declarations.MODE);
							-- MMachine_Guard.MMachine_Guard.Remove(Id,Worker1.Output);
						elsif declarations.ProblemLevel=NoOfMMachs then
								Put_Line("WARNING!! >>>> COMPANY HAS TOO MANY MULTIPLICATION TASKS TO DO! EVERY WORKER HAS IT'S OWN JOB! NO ONE TO HELP WITH BUTTONS");
						else
							Put_Line("Unknown sign!");
							Worker1.Output:=0;
						end if;
						-- Put_Line(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>to the mag"&Natural'Image(Worker1.Id));
				if SHUTorNOT/=SHUTDOWN_BUTTON then 
				 declarations.ItemsMade:=declarations.ItemsMade+1;
				 Magazyn.Magazyn.Insert(Worker1.Output,Id);
				else exit;
				end if;
        delay DelayTimer*declarations.SPEED;
				-- Put_Line(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>leaving loop"&Natural'Image(Worker1.Id));
    end loop;
--============================================================
			-- accept Finish(F:out Natural) do
				-- F:=Worker1.F;
				  Put_Line("Worker["&Natural'Image(Id)&"] - went home.");
			-- end Finish;
			-- Put_Line(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>finished"&Natural'Image(Worker1.Id));
--============================================================
  end Worker1;
--======================================================================WORKER
end Worker;
--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
with Ada.Text_IO,declarations;
use Ada.Text_IO,declarations;
package body MRam is
--============================================================
	  protected body ARam is
		 entry Add(F:in Integer;I:in Integer;Y:in Character) when Count<Index4'Last is
		 begin 
		 B(In_P).x:=F;
		 B(In_P).y:=I;
		 B(In_P).z:=Y;
		 if Y='+' then
		 B(In_P).output:=F+I;
		 elsif Y='-' then
		 B(In_P).output:=F-I;
		 end if;
		 Count:=Count+1;
		 In_P:=In_P+1;
		end Add;
			
		entry Del(Output: out Integer) when Count>0 is
		 begin 
		 Output:=B(Out_P).output;
		 Count:=Count-1;
		 Out_P:=Out_P+1;
		end Del;
		
		procedure Print(I:in Integer) is
		 begin 
		 Put_Line("M1 stock:"&Index4'Image(Count));
		 end Print;
		end ARam;
--============================================================
	 protected body MRam is
			entry Add(F:in Integer;I:in Integer;Y:in Character) when Count<Index3'Last and Pressed=True is
			begin 
				B(In_P).x:=F;
				B(In_P).y:=I;
				B(In_P).z:=Y;
				B(In_P).output:=F*I;
				Count:=Count+1;
				In_P:=In_P+1;
				Pressed:=False;
			end Add;
			
			entry Del(Output: out Integer) when Count>0 is
			begin 
				Output:=B(Out_P).output;
				Count:=Count-1;
				Out_P:=Out_P+1;
				-- Put_Line("3... ... ...M");
			end Del;
			
			procedure Help is
			begin
			if Pressed=False then
				Put_Line("Button press.");
				Pressed:=True;
			else 
				null;
			end if;
			end Help;
			
			procedure Print(I:in Integer) is
			begin 
				Put_Line("M2 stock:"&Index3'Image(Count));
			end Print;
		end MRam;
--============================================================
end MRam;
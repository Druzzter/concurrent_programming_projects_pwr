--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
with declarations;
package MRam is
		protected type ARAM is
			entry Add(F:in Integer; I:in Integer; Y:in Character);
			entry Del(Output:out Integer);
			procedure Print(I:in Integer);
		private
		Output:Integer;
			BeingUsed:Boolean:=False;
			Button:Integer:=0;
			B: declarations.ListaZadan3;
			In_P, Out_P, Count:declarations.Index4:=0;
			Empty :Boolean :=True;
		end ARam;
--============================================================AdditiveRam Memory
		protected type MRam is
			entry Add(F:in Integer; I:in Integer; Y:in Character);
			entry Del(Output:out Integer);
			procedure Help;
			procedure Print(I: in Integer);
		private
			Output:Integer;
			Pressed:Boolean:=False;
			Failed:Boolean:=False;
			Button:Integer:=0;
			B: declarations.ListaZadan2;
			In_P, Out_P, Count:declarations.Index3:=0;
			Empty :Boolean :=True;
	 end MRam;
--============================================================MultiplierRam Memory
end MRam;
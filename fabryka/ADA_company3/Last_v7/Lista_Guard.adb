--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
with Ada.Text_IO, declarations, buffers; 
use Ada.Text_IO, declarations, buffers;

package body Lista_Guard is
	 task body Lista_Guard is
			Q_Size:constant:=NoOfItem;
			subtype Q_Range is Positive range 1..Q_Size;
			Length : Natural range 0..Q_Size:=0;
			Id:Natural:=1;
			Head,Tail :Q_Range := 1;
		begin
			accept Start(Id :in Natural) do
				Lista_Guard.Id:=Id;
				Put_Line("List Keeper came to work early today.");
			end Start;
			while not Finished loop 
				exit when SHUTorNOT=SHUTDOWN_BUTTON;
				select 
					when Length < Q_Size =>
						accept Insert(F:in Integer; I:in Integer; Y:in Character; Nr:in Natural; Mode:in Integer) do
						if SHUTorNOT/=SHUTDOWN_BUTTON then 
							Lista.Add(F,I,Y);
							end if;
							if Mode >0 then
							Put_Line("BOSS["&Natural'Image(Nr)&"] added task {"&Integer'Image(F)&Character'Image(Y)&Integer'Image(I)&"} to the list.");
							else
								null;
							end if;
						end Insert;
										exit when SHUTorNOT=SHUTDOWN_BUTTON;
						Tail:=Tail mod Q_Size+1;
						Length:=Length+1;
				or
					when Length> 0 =>
						accept Remove(F:out Integer; I:out Integer; Y:out Character; Nr :in Natural; Mode:in Integer) do
						if SHUTorNOT/=SHUTDOWN_BUTTON then 
							Lista.Del(F,I,Y);
							end if;
							if Mode>0 then
							Put_Line("WORKER["&Natural'Image(Nr)&"] took task "&Integer'Image(F)&Character'Image(Y)&Integer'Image(I)&" from the list.");
							else
								null;
							end if;
						end Remove;
						exit when SHUTorNOT=SHUTDOWN_BUTTON;
						Head:=Head mod Q_Size+1;
						Length:=Length-1;
				or delay 3.0;
				if declarations.MODE>0 then
					Put_Line("LG is still waiting for workers to take their jobs.");
				end if;
				end select;
			end loop;
			-- Put_Line("ListGuard waiting for finish.");
			-- accept Finish(Id:out Natural) do
				-- Id:=Lista_Guard.Id;
				-- -- !("Lid!end= "&Natural'Image(Id));
			-- end Finish;							
			 Put_Line("List Keeper got bored and went home.");
	end Lista_Guard;
	 --======================================================================LISTA
end Lista_Guard;
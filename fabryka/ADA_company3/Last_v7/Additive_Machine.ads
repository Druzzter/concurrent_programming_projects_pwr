package Additive_Machine is
	task type Additive_Machine is
		entry Start(Id:in Natural);
		-- entry Finish(Id:out Natural);
		-- entry Insert(F:in Integer; I:in Integer; Y:in Character; Nr:in Natural; Mode:in Integer);
		-- entry Remove(F:out Integer;I:out Integer; Y:out Character; Nr:in Natural; Mode:in Integer);
		entry Add( Id:in Natural; F:in Integer; I:in Integer; Y:in Character; Mode:in Integer);
		entry GetAOut(Output:out Integer);
		entry Report(Id:in Natural; MyStat:out Integer);
		entry PlanB;
		entry Req;
	end Additive_Machine;
end Additive_Machine;
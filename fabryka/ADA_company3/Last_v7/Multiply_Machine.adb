with Ada.Text_IO, declarations, buffers, MRam,Ada.Numerics.Discrete_Random; 
use Ada.Text_IO, declarations, buffers, MRam;
--============================================================
package body Multiply_Machine is
--============================================================
	 task body Multiply_Machine is
			Id:Natural;
			B:Button;
			F:Integer;
			I:Integer;
			Y:Character;
			Mode:Integer;
			Output:Integer;
			HelpId:Natural;
			Memory : MRam.MRam;
			Disabled:Boolean;
		begin
--============================================================
			accept Start(Id :in Natural) do
				Multiply_Machine.Id:=Id;
				Put_Line("MMid= "&Natural'Image(Id));
				Multiply_Machine.B:=0;
				Disabled:=False;
			end Start;
--============================================================
			while not Finished loop 
				exit when SHUTorNOT=SHUTDOWN_BUTTON;
				select 
					when Multiply_Machine.B=0 =>
					accept Multiply( Id:in Natural; F:in Integer; I:in Integer; Y:in Character; Mode:in Integer) do
					Multiply_Machine.B:=1;
					declarations.ProblemLevel:=declarations.ProblemLevel+1;
					Multiply_Machine.Id:=Id;
					Multiply_Machine.F:=F;
					Multiply_Machine.I:=I;
					Multiply_Machine.Y:=Y;
					Multiply_Machine.Mode:=Mode;
					if  declarations.MODE>0 then
					Put_Line(".["&Natural'Image(Id)&"]task -needs button press");
					end if;
					end Multiply;
					exit when SHUTorNOT=SHUTDOWN_BUTTON;
				or
					when Multiply_Machine.B=1 =>
					accept Help(Id:in Natural) do
						Multiply_Machine.B:=2;
						Multiply_Machine.HelpId:=Id;
						if declarations.MODE>0 then
						Put_Line(",["&Natural'Image(Id)&"]task -helping with button{"&Natural'Image(Multiply_Machine.Id)&"}");
						end if;
						declarations.ProblemLevel:=declarations.ProblemLevel-1;
					end Help;
				or
					when Multiply_Machine.B=2 =>
					 accept GetMOut(Output:out Integer) do
								   Multiply_Machine.Output:=Multiply_Machine.F*Multiply_Machine.I;
									-- Memory.GetOutput(Multiply_Machine.Output);
									Output:=Multiply_Machine.Output;
									Multiply_Machine.B:=0;
					end GetMOut;
				or accept NeedHelp(B:out Button) do
				B:=Multiply_Machine.B;
				-- Put_Line("NeedHelp:"&Button'Image(B)&"! <=Machine["&Natural'Image(Id)&"]");
				end NeedHelp;
				
				-- or accept PlanB do
					-- Multiply_Machine.B:=0;
				-- end PlanB;
				-- or	delay 2.0;
				-- accept Req do
				 -- requeue PlanB;-- with abort;
				-- end Req;
				end select;
			end loop;
--============================================================
			-- Put_Line("Multiply_Machine waiting for finish.");
			-- accept Finish(Id:out Natural) do
				-- Id:=Multiply_Machine.Id;
				-- -- !("MMid!end= "&Natural'Image(Id));
			-- end Finish;							
		 Put_Line("MMid!end="&Natural'Image(Id));
--============================================================
	end Multiply_Machine;
--======================================================================Multiply_Machine
end Multiply_Machine;
--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
with Ada.Text_IO, declarations, buffers, Observer, Lista_Guard, Magazyn, Boss, Worker, Client, Multiply_Machine, Ada.Characters.Latin_1,Additive_Machine, taskdefinitions,  MRam,Computing_Guard,Adding_Guard,Repair; 
use Ada.Text_IO, declarations, buffers, Observer, Lista_Guard, Magazyn, Boss, Worker, Client, Multiply_Machine, Ada.Characters.Latin_1,Additive_Machine, taskdefinitions, MRam, Computing_Guard,Adding_Guard, Repair;
--============================================================
procedure Boss_Worker_Client is
--============================================================
	-- subtype wk is Worker1; --subtypes for later use (hard problem)
	-- subtype cl is Client;
	 Worker1s : array(1..NoOfWorkers) of Worker.Worker1;
	 Clients : array(1..NoOfClients) of Client.Client;
	 HWorker1s: array(1..NoOfHWorker1s) of Worker.Worker1;
	 
begin
	 -- Observer.Observer.Start(IdObserver);
	 -- AMachine_Guard.AMachine_Guard.Start(IdAMG);
	 -- MMachine_Guard.MMachine_Guard.Start(IdAMG);
	 Magazyn.Magazyn.Start(IdMagazyn);
	 Computing_Guard.Computing_Guard.Start;
	 Adding_Guard.Adding_Guard.Start;
	 Repair.Repair.Start;
	 Lista_Guard.Lista_Guard.Start(IdLista);
	 Boss.Boss.Start(IdBoss);
	for I in 1..NoOfClients loop
	 Clients(I).Start(I);
	end loop;
	for I in 1..NoOfWorkers loop 
	 Worker1s(I).Start(I); 						-- distribute work between workers
	end loop;
--COMPANY IS WORKING FROM NOW ON!
while not Finished loop 									-- main task becomes Observator from prev version.
	exit when SHUTDOWN_BUTTON=SHUTorNOT;
	 Ada.Text_IO.Get_Immediate (Answer);
	 if Answer = Ada.Characters.Latin_1.ESC then
		Finished:=True;
		SHUTorNOT:=SHUTDOWN_BUTTON;
		Put_Line(">>>>>>>>>>>>>>>>>>>>>>>>>Shutdown<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	end if;
	 case Answer is
			when 'Y'|'y' =>
				if declarations.Added=False then
				Put_Line("--{~~~~~~}--");
				Lista.Print(1);
				NewBuffer.Print(1);
				Put_Line("List size => "&Natural'Image(NoOfItem));
				Put_Line("Warehouse size => "&Natural'Image(NoOfItem2));
				Put_Line("#M.Machs => "&Natural'Image(NoOfMMachs));
				Put_Line("#A.Machs => "&Natural'Image(NoOfAMachs));
				Put_Line("# of Workers => "&Integer'Image(NoOfWorkers));
				Put_Line("# of Clients => "&Integer'Image(NoOfClients));
				Put_Line("Global speed => "&Duration'Image(SPEED));
				Put_Line("Problem level => "&Natural'Image(declarations.ProblemLevel));
				Put_Line("--{~~~~~~}--");
				else 
				Put_Line("--{~~~~~~}--");
				Lista.Print(1);
				NewBuffer.Print(1);
				Put_Line("List size => "&Natural'Image(NoOfItem));
				Put_Line("Warehouse size => "&Natural'Image(NoOfItem2));
				Put_Line("#M.Machs => "&Natural'Image(NoOfMMachs));
				Put_Line("#A.Machs => "&Natural'Image(NoOfAMachs));
				Put_Line("# of Workers => "&Integer'Image(NoOfWorkers));
				Put_Line("# of Helping Workers => "&Integer'Image(NoOfHWorker1s));
				Put_Line("# of Clients => "&Integer'Image(NoOfClients));
				Put_Line("Global speed => "&Duration'Image(SPEED));
				Put_Line("Problem level => "&Natural'Image(declarations.ProblemLevel));
				Put_Line("--{~~~~~~}--");
				end if;
			when 'N'|'n' => 
					SHUTorNOT:=SHUTDOWN_BUTTON;
					Put_Line(">>>>>>>>>>>>>>>>>>>>>>>>>Shutdown<<<<<<<<<<<<<<<<<<<<<<<<<<<");
			when '0' =>
				declarations.MODE:=0;
			when '1' =>
				declarations.MODE:=1;
			when '3' =>
				Put_Line("Boss will be helping if he is still in work.");
				declarations.HelpingBossOption:=True;
			when '-' =>
				declarations.SPEED:=declarations.SPEED-1.0;
			when '=' =>
				declarations.SPEED:=declarations.SPEED+1.0;
			when 'r' =>
				Put_Line("Reload machines");
			when 'a' =>
					if declarations.Added=False then
					Put_Line("Adding 10 temporary tranees...");
					for I in 11..NoOfHWorker1s loop 
						HWorker1s(I).Start(I); 						-- distribute work between workers
					end loop;
					declarations.Added:=True;
					else Put_Line("Workers has been added few moments ago.");
					end if;
			when others  => 
			--Put_Line("...");
				null;
	 end case;
	exit when SHUTorNOT=SHUTDOWN_BUTTON;
end loop;

--============================================================CLEAN UP
--catch all tasks ending their jobs.
-- Put_Line("and clients ending.");
-- for I in 1..NoOfClients loop
		-- Clients(I).Finish(declarations.Waste);
-- end loop;
-- Put_Line("Now Worker1s...");
-- for I in 1..NoOfWorkers loop
		-- Worker1s(I).Finish(declarations.Waste);
-- end loop;
-- -- Put_Line("Wait for Observer to quit.");
	-- -- Observer.Observer.Finish(IdObserver);
-- Put_Line("Wait for Boss to quit.");
	-- Boss.Finish(IdBoss);
-- Put_Line("Wait for Lista to quit.");
	-- Lista_Guard.Finish(IdLista);
-- Put_Line("Wait for Magazyn to quit.");
	-- Magazyn.Finish(IdMagazyn);
	-- Boss.Boss.BossHelp;
	 Put_Line("Observer has left.");	
--============================================================
end Boss_Worker_Client;
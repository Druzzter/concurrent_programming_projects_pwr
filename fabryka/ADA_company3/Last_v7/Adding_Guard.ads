--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
package Adding_Guard is
task type Adding_Guard is
		entry Start;
		-- entry Finish(Id:out Natural);
		entry Insert( Nr:in Natural; F:in Integer; I:in Integer; Y:in Character; Mode:in Integer);
		entry Remove(Id:in Natural; Output:out Integer);
		entry GoAndFix;
		entry Pass;
end Adding_Guard;
end Adding_Guard;

--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI

with Ada.Text_IO, declarations, buffers, Lista_Guard, RandGen, Ada.Numerics.Discrete_Random, taskdefinitions; 
use Ada.Text_IO, declarations, buffers, Lista_Guard, RandGen, taskdefinitions;
package body Boss is

	 task body Boss is
	 Id:Natural:=4;
	 Need:Natural;
   package Rand_Int is new Ada.Numerics.Discrete_Random(Integer);
   seed : Rand_Int.Generator;
   Num : Integer;
	 Op:Character:='+';
	 begin
		
			accept Start(Id :in Natural) do
				Boss.Id:=Id;
				Put_Line("WARNING WORKERS! BOSS HAS CAME TO WORK!"&Natural'Image(Id));
				Boss.Need:=0;
			end Start;
			for N in 1..NoOfItem loop
				exit when SHUTorNOT=SHUTDOWN_BUTTON;
				exit when Finished=True;
				if SHUTorNOT/=SHUTDOWN_BUTTON then 
					 Rand_Int.Reset(seed);
					 Num := Rand_Int.Random(seed);
					 Num:=Num mod 1337;
							if Num>0 and Num<334 then Op:='+';
					  elsif Num>333 and Num<668 then Op:='-';
					  elsif Num>667 and Num<999 then Op:='*';
						else Op:='-';
						end if;
					 Num:=Num mod N;
					 Lista_Guard.Lista_Guard.Insert(Num,Num,Op,Id,declarations.MODE);
				 else exit;
				end if;
				 delay StandardSpeed+DelayTimer*declarations.SPEED;
			end loop;
			if  declarations.MODE>0 then
			Put_Line("Is Boss helping?");
			end if;
			-- accept BossHelp do
				if declarations.MODE>0 and declarations.HelpingBossOption=True then
				Put_Line("Boss ran out of his ideas for todays tasks.");
				Put_Line("Boss is preparing to help Workers with their jobs");
				while not Finished loop
					for I in 1..NoOfMMachs loop
					 -- Put_Line(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>helping loop"&Natural'Image(Boss.Id));
						taskdefinitions.MMachines(I).NeedHelp(Boss.Need);
						if Boss.Need>0 then
							if  declarations.MODE>0 then
							 Put_Line("Boss Sending help>"&Natural'Image(I));
							end if;
							taskdefinitions.MMachines(I).Help(Boss.Id); -- FIRST try to help someone... if there is someone..
						end if;
							-- declarations.MRarr(I).Help;
					end loop;
				end loop;
				end if;
			-- end BossHelp;
			-- accept Finish(Id :out Natural) do
				-- Id:=Boss.Id;
				 Put_Line("Boss went home.");
			-- end Finish;
	 end Boss;
	 --======================================================================BOSS
end Boss;
with Ada.Unchecked_Deallocation;
package Worker is
--======================================================================
task type Worker1 is
--======================================================================
		entry Start(Id:in Natural);
		-- entry Finish(F: out Natural);
	 end Worker1;
--======================================================================
	 type PWorker1 is access Worker1;
	 procedure Free is new Ada.Unchecked_Deallocation(Worker1,PWorker1);
--======================================================================
end Worker;
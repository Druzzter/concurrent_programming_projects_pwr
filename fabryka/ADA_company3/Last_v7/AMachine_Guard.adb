with Ada.Text_IO, declarations, taskdefinitions;
use Ada.Text_IO, declarations, taskdefinitions;

package body AMachine_Guard is
	 task body AMachine_Guard is
			-- Q_Size:constant:=NoOfAMachs;
			-- subtype Q_Range is Positive range 1..Q_Size;
			Length : Natural range 0..Q_Size1:=0;
			Id:Natural:=1;
			Head,Tail :Q_Range1 := 1;
			Output:Integer:=0;
			-- StatusAM: array (Q_Range) of Integer;
		begin
			accept Start(Id :in Natural) do
				AMachine_Guard.Id:=Id;
				Put_Line("AMGid= "&Natural'Image(Id));
				for I in 1..NoOfAMachs loop
					AMachines(I).Start(I);
				end loop;
				for J in 1..Q_Size1 loop
					StatusAM(J):=0;
					Put_Line("AMachine"&Integer'Image(J)&" ready!");
				end loop;
				Put_Line("All AMachines are ready!");
			end Start;
			while not Finished loop 
				exit when SHUTorNOT=SHUTDOWN_BUTTON;
				select 
					 when Length < Q_Size1 =>
						accept Insert( Nr:in Natural;F:in Integer; I:in Integer; Y:in Character; Mode:in Integer) do
						if SHUTorNOT/=SHUTDOWN_BUTTON and  declarations.MODE>0 then 
						Put_Line("AMG directed Worker["&Natural'Image(Nr)&"to the AMachine[]");
							for J in 1..Q_Size1 loop
								 -- taskdefinitions.AMachines(J).Report(Nr, StatusAM(J)); --check status of machine!
								 Put_Line("AM["&Natural'Image(J)&"]-status:"&Integer'Image(StatusAM(J)));
								if StatusAM(J)=0 then
									StatusAM(J):=1; -- while machine is making math, it is busy
									if  declarations.MODE>0 then
									Put_Line("{");
									end if;
									taskdefinitions.AMachines(J).Add(Nr,F,I,Y, Mode); -- NA SZTYWNO DO POPRAWY!!!!!!!!!!!!!!!!!!!!!!!
									taskdefinitions.AMachines(J).GetAOut(Output);
									if  declarations.MODE>0 then
									Put_Line("}");
									end if;
									StatusAM(J):=0; -- while machine is making math, it is busy
									exit;
								elsif StatusAM(J)=1 then
									null; -- Add machine is busy right now
								elsif StatusAM(J)=2 then
									null; -- put here repair man!
								end if;
							end loop;
						end if;
						if Mode >0 then
							Put_Line("AM>Worker["&Natural'Image(Nr)&"] added task {"&Integer'Image(F)&Character'Image(Y)&Integer'Image(I)&"} for compute.");
						else
							null;
						end if;
						end Insert;
						exit when SHUTorNOT=SHUTDOWN_BUTTON;
						Tail:=Tail mod Q_Size1+1;
						Length:=Length+1;
				or
					when Length> 0 =>
						accept Remove(Id:in Natural;Output:out Integer) do
						Output:=AMachine_Guard.Output;
						if SHUTorNOT/=SHUTDOWN_BUTTON and  declarations.MODE>0 then 
							Put_Line("AM>Worker["&Natural'Image(Id)&"]Removed his:{"&Integer'Image(Output)&"} from computing");
						end if;
						
					end Remove;
					exit when SHUTorNOT=SHUTDOWN_BUTTON;
					Head:=Head mod Q_Size1+1;
					Length:=Length-1;
				or delay 3.0;
					if declarations.MODE>0 then
						Put_Line("AMG got stuck.");
					end if;
					for J in 1..Q_Size2 loop
					taskdefinitions.MMachines(J).Help(AMachine_Guard.Id);
					end loop;
				end select;
			end loop;
			-- Put_Line("ListGuard waiting for finish.");
			-- accept Finish(Id:out Natural) do
				-- Id:=AMachine_Guard.Id;
				-- -- !("Lid!end= "&Natural'Image(Id));
			-- end Finish;							
			 Put_Line("AMGid!end="&Natural'Image(Id));
	end AMachine_Guard;
	 --======================================================================LISTA
end AMachine_Guard;
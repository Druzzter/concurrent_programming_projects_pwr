--@Author: ARKADIUSZ LEWANDOWSKI
--@Concurrent Programming Course
--@Lecturer: DR. PAWEL ZIELINSKI
package Lista_Guard is
--======================================================================
task type Lista_Guard is
--======================================================================
		entry Start(Id:in Natural);
		-- entry Finish(Id:out Natural);
		entry Insert(F:in Integer; I:in Integer; Y:in Character; Nr:in Natural; Mode:in Integer);
		entry Remove(F:out Integer;I:out Integer; Y:out Character; Nr:in Natural; Mode:in Integer);
--======================================================================
end Lista_Guard;
--======================================================================
end Lista_Guard;
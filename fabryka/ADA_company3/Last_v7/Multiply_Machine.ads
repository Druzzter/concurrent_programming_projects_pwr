with declarations;
package Multiply_Machine is
	task type Multiply_Machine is
		entry Start(Id:in Natural);
		-- entry Finish(Id:out Natural);
		-- entry Insert(F:in Integer; I:in Integer; Y:in Character; Nr:in Natural; Mode:in Integer);
		-- entry Remove(F:out Integer;I:out Integer; Y:out Character; Nr:in Natural; Mode:in Integer);
		entry Multiply( Id:in Natural; F:in Integer; I:in Integer; Y:in Character; Mode:in Integer);
		entry GetMOut( Output:out Integer);
		entry Help(Id:in Natural);
		entry NeedHelp(B:out declarations.Button);

	end Multiply_Machine;
end Multiply_Machine;
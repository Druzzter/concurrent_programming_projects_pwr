package routines
 import (		
				"folderStructs"
				"folderConstants"
				"folderFunction"
				 "sync"
)
//===========================================================================================================================
func Company(){ // WHOLE COMPANY PREPARES EVERYTHING TO WORK CONCURRENTLY VIA CHANNELS
	functions.ClearScreen() // CLEAR SCREEN FOR BETTER OUTPUT
	cLb:=make(chan structs.Task) // CHANNEL FOR List-Boss 
	cLw:=make(chan structs.Task) // CHANNEL fOR List-Worker
	cMw:=make(chan int)					 // CHANNEL FoR Magazyn-Worker
	cAMwi:=make(chan structs.Task)// CHANNEL fOr Worker-AMG
	// cWam:=make(chan int) 					// CHANNEL FOR AMG-Worker
	cMMwi:=make(chan structs.Task)// CHANNEL FOR Worker-MMG
	cB:=make(chan int)
	// cWmm:=make(chan int)					//CHANNEL FOR MMG-Worker
	cAMamg:=make(chan structs.Task)
	cAMwo:=make(chan int)
	cMMmmg:=make(chan structs.Task)
	cMMr:=make(chan structs.Task)
	cAMr:=make(chan structs.Task)
	cRwo:=make(chan structs.Task)
	cMMwo:=make(chan int)
	cMc:=make(chan int)					 // CHANNEL foR Magazyn-Client
	Press:=make(chan bool)
	var wg sync.WaitGroup				 // ALL TASKS BELONGS TO ONE WaitGroup for Observer to type true data
	wg.Add(1);									 // WAITGROUP GOT ONE TASK TO WAIT FOR
	//SPECIALLY REVERSED START OF ALL TASKS
	for i:=0; i<constants.NumOfClients; i++{ //START EVERY CLIENT
		go functions.Client(&wg,cMc,i)
	}
	go functions.RepairMan(&wg, cMMr, cAMr, cRwo, constants.REPAIR)
	go functions.Magazyn_Guard(&wg,cMw,cMc)  // START MAGAZYN GUARD
	go functions.AMachine_Guard(&wg, cAMwi, cAMamg)//START AddMachine Guard
	for i:=0; i<constants.NumOfAM; i++ {
		go functions.AMachine(&wg,cAMamg,cAMwo, cAMr,cRwo, i)
	}
	go functions.MMachine_Guard(&wg, cMMwi, cMMmmg, cB)// START Multiplication Machine Guard
	for i:=0; i<constants.NumOfMM; i++ {
		go functions.MMachine(&wg, cMMmmg, cMMwo, cMMr, cRwo, i)
	}
	go functions.Lista_Guard(&wg,cLb,cLw)		 // START LISTA GUARD
	go functions.Observer(&wg,cLb,cLw,cMw,cMc)//START OBSERVER
	for i:=0; i<constants.NumOfWorkers; i++ { //START eVERY WORKER
		go functions.Worker(&wg,cLw,cMw,cAMwi,cAMwo,cMMwi,cMMwo, Press, i)
	}
	go functions.Boss(&wg,cLb)								//START BOSS
}
//===========================================================================================================================
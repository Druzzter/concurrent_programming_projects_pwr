package functions
 import (
				"fmt"						
				"folderConstants"
				"folderStructs"
				"math/rand"
				"time"
				"sync"
)
//===========================================================================================================================
func MMachine(wg *sync.WaitGroup, cMMmmg <-chan structs.Task, cMMwo chan<- int, cMMr chan structs.Task, cRwo chan structs.Task, Id int){	// MAGAZYN GUARD STORES PRODUCTS FROM WORKERS via CHANNELS
	var output int
	var probab int
	for {
		wg.Wait()																															//WaitGroup with other THREADS
			select {
				case fromWorker:= <-cMMmmg:																				//WHEN SOMETHING COMES FROM CHANNEL CONNECTED WITH WORKER
				// fmt.Println(fromWorker)
				if constants.MODE {																								//VERBOSE MODE MORE DATA
					fmt.Println("MM[",Id,"] received from Worker: ",fromWorker)						// DIsPLAY THis MESSAGE
				}	else {
					// fmt.Println("AMG received product")
				}
				probab=rand.Intn(10000)
				if probab < 300 {
					fmt.Println("WARNING MACHINE CRASHED! NEEDS REPAIR!")
					cMMr <- fromWorker
					time.Sleep(time.Duration(rand.Int31n(1000)) * time.Millisecond)
				} else {
				output = fromWorker.Operation()
				time.Sleep(time.Duration(rand.Int31n(1000)) * time.Millisecond)
				cMMwo<-output																										//SeND IT THROUGH NEXT CHANNEL PREPARED ONLY FOR WORKERS
				if constants.MODE {																								//SHOW WHAT HAS BEEN SENT
					fmt.Println("MM[",Id,"] sent back to the channel, for Workers :", output)
				}	else {
					// fmt.Println("AMG sent to the channel.")
					Display(0,constants.AMGUARD)																		//SILENT MODE MORE VISUAL
				}
				}
				case fromRep:= <-cRwo:
				fmt.Println("------------------------------",fromRep)
				if constants.MODE {																								//VERBOSE MODE MORE DATA
					fmt.Println("MM[",Id,"] received from Worker: ",fromRep)						// DIsPLAY THis MESSAGE
				}	else {
					// fmt.Println("AMG received product")
				}
				probab=rand.Intn(10000)
				if probab < 3 {
					fmt.Println("WARNING MACHINE CRASHED! NEEDS REPAIR!")
					cMMr <- fromRep
					time.Sleep(time.Duration(rand.Int31n(1000)) * time.Millisecond)
				} else {
				output = fromRep.Operation()
				time.Sleep(time.Duration(rand.Int31n(1000)) * time.Millisecond)
				cMMwo<-output																										//SeND IT THROUGH NEXT CHANNEL PREPARED ONLY FOR WORKERS
				if constants.MODE {																								//SHOW WHAT HAS BEEN SENT
					fmt.Println("MM[",Id,"] sent back to the channel, for Workers :", output)
				}	else {
					// fmt.Println("AMG sent to the channel.")
					Display(0,constants.AMGUARD)																		//SILENT MODE MORE VISUAL
				}
				}
			}
	}
}
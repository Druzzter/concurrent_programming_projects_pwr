package functions
import (
				"fmt"						
				"folderConstants"
				"os"
				"os/exec"
)
//===========================================================================================================================
func Display(pos int , who int){ 					//DISPLAYS LIST AND MAGAZYN 
	if who > constants.MGUARD {							//DIspLAY CONTAINERS OR EMPLOYEES
		if who == constants.BOSS {
			fmt.Println("Boss sent X+Y")
		} 
		if who == constants.WORKER {
			fmt.Println("Worker got X+Y")
			fmt.Println("Worker sent X+Y")
		}
		if who == constants.CLIENT {
			fmt.Println("Client got Z")
			// ClearScreen()
		}
		if who == constants.AMGUARD {
			fmt.Println("Worker computing +Z")
			fmt.Println("AMG sending back Z")
		}
		if who == constants.MMGUARD {
			fmt.Println("Worker computing *Z")
			fmt.Println("MMG sending back *Z")
		}
	} else {
		if who == constants.LGUARD {
			fmt.Print("Lista|")
			for i:=0; i<constants.CMDlen1; i++ {
				if i==pos {
					fmt.Print("x,")
				} else {
					fmt.Print("_,")
				}
			}
			fmt.Println("|length: ",constants.CMDlen1)
		}
		if who == constants.MGUARD {
			fmt.Print("Magazyn|")
			for i:=0; i<constants.CMDlen2; i++ {
				if i==pos {
					fmt.Print("x,")
				} else {
					fmt.Print("_,")
				}
			}
			fmt.Println("|length: ",constants.CMDlen2)
		}
	}
}
//===========================================================================================================================
func ClearScreen(){								// FUNCTION FOR CLEARING SCREEN USING windows CMD
			cmd := exec.Command("cmd", "/c", "cls")
			cmd.Stdout = os.Stdout
			cmd.Run()
}
//===========================================================================================================================
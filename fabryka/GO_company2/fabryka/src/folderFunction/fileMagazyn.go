package functions
 import (
				"fmt"						
				"folderConstants"
				"sync"
)
//===========================================================================================================================
func Magazyn_Guard(wg *sync.WaitGroup, cMw <-chan int, cMc chan<- int){	// MAGAZYN GUARD STORES PRODUCTS FROM WORKERS via CHANNELS
	var mag [constants.MagazynSize]int																		// MAGAZYN SLICE
	for {
		wg.Wait()																														//WaitGroup with other THREADS
		magazyn := mag[0:]
		for i:=0; i<constants.MagazynSize; i++ {														//ITERATE OVER AND OVER THROUGH MAGAZYN (SATURATE)
			select {
				case fromWorker:= <-cMw:																				//WHEN SOMETHING COMES FROM CHANNEL CONNECTED WITH WORKER
				magazyn[i]=fromWorker																						//ASSIGN MESSAGE/PRODUCT TO MAGAZYN
				if constants.MODE {																							//VERBOSE MODE MORE DATA
					fmt.Println("MagazynGuard received from Worker:",magazyn[i])	// DIsPLAY THis MESSAGE
				}	else {
					// fmt.Println("MagazynGuard received product")
				}
				cMc<-magazyn[i]																									//SeND IT THROUGH NEXT CHANNEL PREPARED ONLY FOR CLIENTS
				if constants.MODE {																							//SHOW WHAT HAS BEEN SENT
					fmt.Println("MagazynGuard sent to the channel, for Clients", fromWorker)
				}	else {
					// fmt.Println("MagazynGuard sent to the channel.")
					Display(i,constants.MGUARD)																		//SILENT MODE MORE VISUAL
				}
				
			}
		}
	}
}
//===========================================================================================================================
package functions
 import (
				"fmt"						
				"folderStructs"
				"folderConstants"
				"time"
				"sync"
)
//===========================================================================================================================
func Observer(wg *sync.WaitGroup, cLb<-chan structs.Task, cLw<-chan structs.Task, cMw<-chan int, cMc<-chan int){
	// OBSERVER SEEKS FOR NEXT STEPS OF PROGRAM
	var prompter rune ='0';
	time.Sleep(time.Second*constants.SleepTimeForObserverPrompt)
	fmt.Println("Type a key to start: ")
	for {
	fmt.Scan(&prompter)
		if prompter!='0'{ break }
	}
	wg.Done()
}
//===========================================================================================================================
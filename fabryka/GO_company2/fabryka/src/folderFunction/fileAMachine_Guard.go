package functions
 import (
				"fmt"						
				"folderConstants"
				"folderStructs"
				"sync"
)
//===========================================================================================================================
func AMachine_Guard(wg *sync.WaitGroup, cAMwi <-chan structs.Task, cAMamg chan<- structs.Task){	// MAGAZYN GUARD STORES PRODUCTS FROM WORKERS via CHANNELS
	
	for {
		wg.Wait()																															//WaitGroup with other THREADS
			select {
				case fromWorker:= <-cAMwi:																				//WHEN SOMETHING COMES FROM CHANNEL CONNECTED WITH WORKER
				fmt.Println(fromWorker)
				if constants.MODE {																								//VERBOSE MODE MORE DATA
					fmt.Println("AMG received from Worker: ",fromWorker)						// DIsPLAY THis MESSAGE
				}	else {
					// fmt.Println("AMG received product")
				}
				// output = fromWorker.Operation()
				cAMamg<-fromWorker																										//SeND IT THROUGH NEXT CHANNEL PREPARED ONLY FOR WORKERS
				if constants.MODE {																								//SHOW WHAT HAS BEEN SENT
					fmt.Println("AMG sent back to the channel, for Workers :", fromWorker)
				}	else {
					// fmt.Println("AMG sent to the channel.")
					Display(0,constants.AMGUARD)																		//SILENT MODE MORE VISUAL
				}
				
			}
	}
}
/*
for i:=0; i<constants.NumOfWorkers; i++ { //START eVERY WORKER
		go functions.Worker(&wg,cLw,cMw, i)
	}
*/
//===========================================================================================================================
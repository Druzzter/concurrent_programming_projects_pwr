package functions
 import (
				"fmt"						
				"folderStructs"
				"folderConstants"
				"time"
				"sync/atomic"
				"sync"
				"math/rand"
)
//===========================================================================================================================
func Boss(wg *sync.WaitGroup, cLb chan<- structs.Task) int{
// BOSS IS CREATING TASKS FOR HIS WORKERS ON LIST through LISTA GUARD
	i:=0
	var temp structs.Task
	structs.OperatorInit()
	for i < constants.MaxTasksToDo {
		temp.Id=i
		temp.Farg=rand.Intn(100)+1
		temp.Sarg=rand.Intn(100)+1
		temp.Opi=rand.Intn(3)
		i=i+1
		cLb<-temp
		if constants.MODE {
			fmt.Println("Boss sent task to the Lista_Guard:",temp)
		} else {
			// fmt.Println("Boss sent task.")
			Display(1,constants.BOSS)
		}
		atomic.AddUint64(&constants.WaitForObserver,1)
		atomic.AddUint64(&constants.WaitForObserver,1)
		time.Sleep(time.Second * constants.BossSpeed);
	}
	return i;
}		
//===========================================================================================================================
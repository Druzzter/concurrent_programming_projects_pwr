package functions
 import (
				"fmt"						
				"folderConstants"
				"folderStructs"
				"time"
				"math/rand"
				"sync"
)
//===========================================================================================================================
func AMachine(wg *sync.WaitGroup, cAMamg <-chan structs.Task, cAMwo chan<- int, cAMr chan structs.Task, cRwo chan structs.Task, Id int){	// MAGAZYN GUARD STORES PRODUCTS FROM WORKERS via CHANNELS
	var output int
	var probab int
	for {
		wg.Wait()																															//WaitGroup with other THREADS
			select {
				case fromWorker:= <-cAMamg:																				//WHEN SOMETHING COMES FROM CHANNEL CONNECTED WITH WORKER
				// fmt.Println(fromWorker)
				if constants.MODE {																								//VERBOSE MODE MORE DATA
					fmt.Println("AM[",Id,"] received from Worker: ",fromWorker)						// DIsPLAY THis MESSAGE
				}	else {
					// fmt.Println("AMG received product")
				}
				probab=rand.Intn(10000)
				if probab < 300 {
					fmt.Println("WARNING MACHINE CRASHED! NEEDS REPAIR!")
					cAMr <- fromWorker
					time.Sleep(time.Duration(rand.Int31n(1000)) * time.Millisecond)
				} else {
				output = fromWorker.Operation()
				time.Sleep(time.Duration(rand.Int31n(1000)) * time.Millisecond)
				cAMwo<-output																										//SeND IT THROUGH NEXT CHANNEL PREPARED ONLY FOR WORKERS
				if constants.MODE {																								//SHOW WHAT HAS BEEN SENT
					fmt.Println("AM[",Id,"] sent back to the channel, for Workers :", output)
				}	else {
					// fmt.Println("AMG sent to the channel.")
					Display(0,constants.AMGUARD)																		//SILENT MODE MORE VISUAL
				}
				}
			}
	}
}
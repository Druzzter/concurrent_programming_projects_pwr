package functions
 import (
				"fmt"						
				"folderConstants"
				"folderStructs"
				"math/rand"
				"time"
				"sync"
)
//===========================================================================================================================
func RepairMan(wg *sync.WaitGroup, cMMr <-chan structs.Task,cAMr <-chan structs.Task, cRwo chan<- structs.Task, Id int){	//RepairMan for crashed Machines
	// var output int
	
	for {
		wg.Wait()																															//WaitGroup with other THREADS
			select {
				case fromMM:= <-cMMr:																				//WHEN SOMETHING COMES FROM CHANNEL CONNECTED WITH Machine
				fmt.Println(fromMM)
				if constants.MODE {																								//VERBOSE MODE MORE DATA
					fmt.Println("RepairMan[",Id,"] received from MM: ",fromMM)						// DIsPLAY THis MESSAGE
				}	else {
					// fmt.Println("AMG received product")
				}
				// output = fromMM.Operation()
				time.Sleep(time.Duration(rand.Int31n(1000)) * time.Millisecond)
				cRwo<-fromMM																										//SeND IT THROUGH NEXT CHANNEL PREPARED ONLY FOR MachineMult
				if constants.MODE {																								//SHOW WHAT HAS BEEN SENT
					fmt.Println("RepairMan[",Id,"] sent back to the channel, for new MM :", fromMM)
				}	else {
					// fmt.Println("AMG sent to the channel.")
					Display(0,constants.AMGUARD)																		//SILENT MODE MORE VISUAL
				}
				case fromAM:= <-cAMr:
				fmt.Println(fromAM)
				if constants.MODE {																								//VERBOSE MODE MORE DATA
					fmt.Println("RepairMan[",Id,"] received from Worker: ",fromAM)						// DIsPLAY THis MESSAGE
				}	else {
					// fmt.Println("AMG received product")
				}
				// output = fromAM.Operation()
				time.Sleep(time.Duration(rand.Int31n(1000)) * time.Millisecond)
				cRwo<-fromAM																										//SeND IT THROUGH NEXT CHANNEL PREPARED ONLY FOR RepairMan and Machines
				if constants.MODE {																								//SHOW WHAT HAS BEEN SENT
					fmt.Println("RepairMan[",Id,"] sent back to the channel, for new AM :", fromAM)
				}	else {
					// fmt.Println("AMG sent to the channel.")
					Display(0,constants.AMGUARD)																		//SILENT MODE MORE VISUAL
				}
			}
	}
}
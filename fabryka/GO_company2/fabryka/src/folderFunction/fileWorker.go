package functions
 import (
				"fmt"						
				"folderStructs"
				"folderConstants"
				"time"
				 "sync/atomic"
				"sync"
)
//===========================================================================================================================
func Worker(wg *sync.WaitGroup, cLw <-chan structs.Task, cWm chan<- int	, cAMwi chan<- structs.Task, cAMwo <-chan int, cMMwi chan<- structs.Task, cMMwo <-chan int, Press chan bool , Id int){ // WORKER SOLVING TASKS FROM BOSS
	for {																																						 //ENDLESS LOOP
		
		
		select {																																			 //CHOOSE BETWEEN:
			case fromLista_Guard:= <-cLw:																								 //WHEN SOMETHING FROM CHANNEL STRAIGHTLY FROM LIST
				if constants.MODE {																												 //DISPATCH MODE:
					 fmt.Println("Worker[",Id,"]:",fromLista_Guard)													 //VERBOSe MODE WITH MORE DATA
				} else {
					// fmt.Println("Worker took task from the list")
				}
			 // cWm <- fromLista_Guard.Operation()																					 //SOLVE AND SEND SOLVED TASK TO CHANNEL FOR MAGAZYN GUARD
			 if fromLista_Guard.Opi != 2 {
			cAMwi <- fromLista_Guard
			fmt.Println("Sent for computing: ", fromLista_Guard)
				fromAMG:= <- cAMwo
				cWm <- fromAMG
				} else {
				
				
				cMMwi <- fromLista_Guard
				fmt.Println("Sent for computing: ", fromLista_Guard)
				fromMMG:= <- cMMwo
				cWm <- fromMMG
				}
				if constants.MODE {
					fmt.Println("Worker[",Id,"] sent task to the magazyn.",fromLista_Guard.Operation())
				}	else {
					// fmt.Println("Worker sent task.")
					Display(1,constants.WORKER)																							 //SILENT MODE MORE VISUAL
				}
			case Pressed:= <-Press:
				fmt.Println("Button pressed",Pressed)
				
				atomic.AddUint64(&constants.WaitForObserver,1)
				atomic.AddUint64(&constants.WaitForObserver,1)
			time.Sleep(time.Second * constants.WorkerSpeed)																//DELAY WORKERS TIME
		}
	}
}
//===========================================================================================================================
package functions
 import (
				"fmt"						
				"folderConstants"
				"folderStructs"
				"sync"
)
//===========================================================================================================================
func MMachine_Guard(wg *sync.WaitGroup, cMMwi <-chan structs.Task, cMMmmg chan<- structs.Task, cB chan<- int ){	//Multiplication computers' guard
	
	for {
		wg.Wait()																															//WaitGroup with other THREADS
			select {
				case fromWorker:= <-cMMwi:																				//WHEN SOMETHING COMES FROM CHANNEL CONNECTED WITH WORKER
				fmt.Println(fromWorker)
				if constants.MODE {																								//VERBOSE MODE MORE DATA
					fmt.Println("MMG received from Worker: ",fromWorker)						// DIsPLAY THis MESSAGE
				}	else {
					// fmt.Println("AMG received product")
				}
				// output = fromWorker.Operation()
				cMMmmg<-fromWorker																										//SeND IT THROUGH NEXT CHANNEL PREPARED ONLY FOR WORKERS
				if constants.MODE {																								//SHOW WHAT HAS BEEN SENT
					fmt.Println("MMG sent back to the channel, for Workers :", fromWorker)
				}	else {
					// fmt.Println("AMG sent to the channel.")
					Display(0,constants.MMGUARD)																		//SILENT MODE MORE VISUAL
				}
				
			}
	}
}
/*
for i:=0; i<constants.NumOfWorkers; i++ { //START eVERY WORKER
		go functions.Worker(&wg,cLw,cMw, i)
	}
*/
//===========================================================================================================================
package structs
import s "strconv"
import "folderConstants"
//===========================================================================================================================
var Operator [constants.NumOfOperators]string //Is designed to hold all Possible Operators for struct Task
//===========================================================================================================================
type Task struct {
	Id int				//Id to recognise a task in list
	Farg int			//First ARGument for arithmetic operation
	Sarg int			//Second ARGument for arithmetic operation
	Op string			//Operator - arithmetic operation for arguments
	Opi int				//which one operator is it, for simpler use: 0=+ 1=- 2=*
}
//===========================================================================================================================
type Magazyn struct{
	Product int		//Product of a Task
}
//===========================================================================================================================
type List struct {
	List1 []Task;	//List of Tasks
}	
//===========================================================================================================================
func OperatorInit(){
	Operator[0]="+" // OPERATORS FOR Tasks
	Operator[1]="-"
	Operator[2]="*"
}
//===========================================================================================================================
func (t *Task) Operation() int{
	switch t.Opi { // CASE THROUGH ARITHMETIC OPERATORS VIA t.Opi
	case 0:
		return t.Farg+t.Sarg;
	case 1:
		return t.Farg-t.Sarg;
	case 2:
		return t.Farg*t.Sarg;
	}
	return 0;
}
//===========================================================================================================================
func (t *Task) PrintTask() string { // PRINTABLE TASK
return s.Itoa(t.Farg)+t.Op+s.Itoa(t.Sarg);
}
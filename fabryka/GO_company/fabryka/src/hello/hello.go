package main
import ( // GLOBAL FUNCTIONS ARE UPPER CASE
		"fmt"
		"folderConstants"
		"time"
		"folderGoRoutines"
		"flag"
		)

func main(){
	numbPtr := flag.Bool("verbose", false, "a bool")
	listPtr := flag.Int("lista size", constants.ListaSize, "an int")
	magPtr := flag.Int("mag size", constants.MagazynSize, "an int")
	var svar string 
	flag.StringVar(&svar,"svar","bar", "a string var")
	flag.Parse()
	fmt.Println("mode: ", *numbPtr)
	fmt.Println("mode: ", *listPtr)
	fmt.Println("mode: ", *magPtr)
	fmt.Println("svar: ", svar)
	constants.MODE=*numbPtr
	constants.ListaSize1=*listPtr
	constants.MagazynSize1=*magPtr
	routines.Company() // starting all go routines and their channels
	fmt.Println("From Now whole Company is working for your GRADE, Arek.")
	fmt.Println("Do not mess it up.")
	time.Sleep(constants.TimeLeftSec *constants.TimeLeftMin * time.Second) // this delay is designed to not exit program before action takes place.
}
package main
import (
		"fmt" 						// FOR PRINTING
		"folderConstants" // FOR CONSTANT ARGUMeNTS
		"time" 						// FOR TIME SLEEP / DELAY
		"folderGoRoutines"// FOR GO ROUTINES OF MY OWN
		"flag"						// FOR READING FROM ARGUMENTS CMD
		)
//===============================================================================================================================================
func main(){
	numbPtr := flag.Bool("verbose", false, "a bool")									// VERBOSE MODE 
	listPtr := flag.Int("lista size", constants.ListaSize, "an int")  // LISTA SIZE 
	magPtr := flag.Int("mag size", constants.MagazynSize, "an int")   // MAGAZYN SIZE
	var svar string 																									// ALL ARGS from CMD
	flag.StringVar(&svar,"svar","bar", "a string var")
	flag.Parse()																											// PARSE ARGS 
	fmt.Println("mode: ", *numbPtr)
	fmt.Println("mode: ", *listPtr)
	fmt.Println("mode: ", *magPtr)
	fmt.Println("svar: ", svar)
	constants.MODE=*numbPtr																						//ASIGN VARIABLES
	constants.ListaSize1=*listPtr
	constants.MagazynSize1=*magPtr
	routines.Company() // starting all go routines and their channels // START COMPANY
	fmt.Println("From Now whole Company is working for your GRADE, Arek.")
	fmt.Println("Do not mess it up.")
	time.Sleep(constants.TimeLeftSec *constants.TimeLeftMin * time.Second) // this delay is designed to not exit program before action takes place.
}
//===============================================================================================================================================
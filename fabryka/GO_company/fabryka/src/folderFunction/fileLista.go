package functions
 import (
				"fmt"						
				"folderStructs"
				"folderConstants"
				"sync"
)
//===========================================================================================================================
func Lista_Guard(wg *sync.WaitGroup, cLb <-chan structs.Task, cLw chan<- structs.Task) int{ //GUARD FOR LIST
	var listay  [constants.ListaSize]structs.Task
		for {																																//ENDLESS LOOP
			wg.Wait()
			for i:=0; i < constants.ListaSize1; i++ {													//LOOP THROUGH LIST OVER AND OVER (SATURATION)
				select {																												//CHOOSE BETWEEN:
					case fromBoss:= <-cLb:																				//ON  RECEIVE FROM BOSS
							listay[i]=fromBoss																				//ASSIGN MESSAGE FROM BOSS TO THE LIST
								if constants.MODE{																			//GET RECT FOR MODE TO DISPLAY OUTPUTS
									fmt.Println("Lista[",i,"] got new task:",fromBoss)		//MODE VERBOSIVE
								} else {
									// fmt.Println("Lista got new task.")
									Display(i,constants.LGUARD)														//SILENT MODE LESS INFO MORE VISUAL
								}
					cLw <- listay[i]																							//SEND IT THROUGH CHANNEL TO NEXT WAITING WORKER 	
				}
			}
		}		
		return constants.MaxTrades;
}
//===========================================================================================================================
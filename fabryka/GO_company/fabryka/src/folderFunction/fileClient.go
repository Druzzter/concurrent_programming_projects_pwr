package functions
 import (
				"fmt"						
				"folderConstants"
				"time"
				"sync/atomic"
				"sync"
				"math/rand"
)
//===========================================================================================================================
func Client(wg *sync.WaitGroup, cMc <-chan int, Id int){
// CLIENT IS BUYING PRODUCTS FROM MAGAZYN GUARD
	for {
		select {
			case fromMagazyn_Guard:= <-cMc:
				if constants.MODE {
					fmt.Println("Client[",Id,"] got product from MagazynG:",fromMagazyn_Guard)
				}	else {
					// fmt.Println("Client got Product.")
					Display(1,constants.CLIENT)
				}
				atomic.AddUint64(&constants.WaitForObserver,1)
				atomic.AddUint64(&constants.WaitForObserver,1)
			time.Sleep(time.Second * constants.ClientSpeed * time.Duration(rand.Intn(constants.SPEEDrange)))
		}
	}
}
//===========================================================================================================================
package constants
import "time"
//===========================================================================================================================
const NumOfWorkers int = 5
const NumOfClients int = 11
//===========================================================================================================================
const DisplayTime time.Duration = 5
const SPEED int = 1
const SPEEDrange int = 1
const BossSpeed time.Duration = 1
const WorkerSpeed time.Duration = 1
const ClientSpeed time.Duration = 1
const TimeLeftSec time.Duration = 60
const TimeLeftMin time.Duration = 60
const SleepTimeForObserverPrompt time.Duration = 3
//===========================================================================================================================
var MODE bool =false
var WaitForObserver uint64 =2	
//===========================================================================================================================
const MaxTasksToDo int =200
const MaxTrades int = MaxTasksToDo*2
//===========================================================================================================================
const NumOfOperators int =3
//===========================================================================================================================
const PerPrompt int =20
//===========================================================================================================================
const ListaSize int =10
const MagazynSize int =20
var ListaSize1 int =ListaSize
var MagazynSize1 int =MagazynSize
//===========================================================================================================================
const LGUARD int = 1
const MGUARD int = 2
const BOSS int = 3
const WORKER int = 4
const CLIENT int = 5
const CMDlen1 int = ListaSize
const CMDlen2 int = MagazynSize
//===========================================================================================================================
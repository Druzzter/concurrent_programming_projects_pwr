package routines
 import (		
				"folderStructs"
				"folderConstants"
				"folderFunction"
				 "sync"
)
//===========================================================================================================================
func Company(){ // WHOLE COMPANY PREPARES EVERYTHING TO WORK CONCURRENTLY VIA CHANNELS
	functions.ClearScreen() // CLEAR SCREEN FOR BETTER OUTPUT
	cLb:=make(chan structs.Task) // CHANNEL FOR List-Boss
	cLw:=make(chan structs.Task) // CHANNEL fOR List-Worker
	cMw:=make(chan int)					 // CHANNEL FoR Magazyn-Worker
	cMc:=make(chan int)					 // CHANNEL foR Magazyn-Client
	var wg sync.WaitGroup				 // ALL TASKS BELONGS TO ONE WaitGroup for Observer to type true data
	wg.Add(1);									 // WAITGROUP GOT ONE TASK TO WAIT FOR
	//SPECIALLY REVERSED START OF ALL TASKS
	for i:=0; i<constants.NumOfClients; i++{ //START EVERY CLIENT
		go functions.Client(&wg,cMc,i)
	}
	go functions.Magazyn_Guard(&wg,cMw,cMc)  // START MAGAZYN GUARD
	go functions.Lista_Guard(&wg,cLb,cLw)		 // START LISTA GUARD
	go functions.Observer(&wg,cLb,cLw,cMw,cMc)//START OBSERVER
	for i:=0; i<constants.NumOfWorkers; i++ { //START eVERY WORKER
		go functions.Worker(&wg,cLw,cMw, i)
	}
	go functions.Boss(&wg,cLb)								//START BOSS
}
//===========================================================================================================================